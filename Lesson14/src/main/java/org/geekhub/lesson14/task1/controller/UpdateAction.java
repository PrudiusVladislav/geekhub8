package org.geekhub.lesson14.task1.controller;


import org.geekhub.lesson14.task1.model.Source;

public class UpdateAction implements Action {
    @Override
    public void apply(Source source, String name, String value) {
        source.put(name, value);
    }
}
