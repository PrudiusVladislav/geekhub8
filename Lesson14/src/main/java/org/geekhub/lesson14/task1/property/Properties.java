package org.geekhub.lesson14.task1.property;

public interface Properties {
    String getProperty(String name);
}
