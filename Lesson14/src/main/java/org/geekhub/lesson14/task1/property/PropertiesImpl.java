package org.geekhub.lesson14.task1.property;

import org.geekhub.lesson10.logger.Logger;

import java.io.IOException;
import java.io.InputStream;

public class PropertiesImpl implements Properties {
    private Logger logger;

    @Override
    public String getProperty(String name) {
        java.util.Properties property = new java.util.Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        try(InputStream stream =  loader.getResourceAsStream("/config.properties")) {
            property.load(stream);

            return (String) property.get(name);
        } catch (IOException e) {
            logger.logInfo("File not found");
            throw new IllegalArgumentException("File not found", e);
        }
    }
}
