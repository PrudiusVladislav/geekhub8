package org.geekhub.lesson14.task1.view;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;

@WebFilter(filterName = "weekendFilter")
public class WeekendFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        checkDay();
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) {
        filterConfig.getInitParameter("error");
    }

    private static void checkDay() {
        LocalDate now = LocalDate.now();
        DayOfWeek day = now.getDayOfWeek();

        if (day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY) {
            throw new IllegalArgumentException("Incorrect day of week" + day);
        }
    }
}
