package org.geekhub.lesson14.task1.view;

import org.geekhub.lesson10.logger.ConsoleLogger;
import org.geekhub.lesson10.logger.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(filterName = "webBrowserFilter")
public class WebBrowserFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
        filterConfig.getInitParameter("error");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Logger logger = new ConsoleLogger(WebBrowserFilter.class);
        try {
            checkBrowser(request);
        } catch (IllegalArgumentException e) {
            logger.logError(e, "Incorrect browser");
        }

        chain.doFilter(request, response);
    }

    private static void checkBrowser(ServletRequest request) {
        String header = ((HttpServletRequest) request).getHeader("user-agent");
        System.out.println(header);
        if (!header.contains("Edge") || !header.contains("Chrome")) {
            throw new IllegalArgumentException("Incorrect browser");
        }
    }
}