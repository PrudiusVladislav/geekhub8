package org.geekhub.lesson14.task1.model;

import org.geekhub.lesson14.task1.Student;

import java.util.List;


public interface Source {
    void put(String name, String value);

    void delete(String name);

    void invalidate();

    List<Student> getData();

}
