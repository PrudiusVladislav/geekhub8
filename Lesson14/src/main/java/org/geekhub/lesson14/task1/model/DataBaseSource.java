package org.geekhub.lesson14.task1.model;

import org.geekhub.lesson10.logger.ConsoleLogger;
import org.geekhub.lesson10.logger.Logger;
import org.geekhub.lesson14.task1.Student;
import org.geekhub.lesson14.task1.property.Properties;
import org.geekhub.lesson14.task1.property.PropertiesImpl;
import org.h2.Driver;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
public class DataBaseSource implements Source {
    static Logger logger = new ConsoleLogger(DataBaseSource.class);
    Properties properties = new PropertiesImpl();

    int id = 1;
    private final String URL = properties.getProperty("db.host");
    private final String LOGIN = properties.getProperty("db.login");
    private final String PASSWORD = properties.getProperty("db.password");

    public DataBaseSource() {
        Driver.load();
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD)) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "CREATE TABLE STUDENT(ID INT AUTO_INCREMENT PRIMARY KEY,  FIRST_NAME VARCHAR(255), LAST_NAME VARCHAR(255))"
            );
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.logError(e, "Data Base dont create");
            throw new IllegalArgumentException("Data Base dont create: ", e);
        }
    }

    @Override
    public void put(String name, String value) {
        Driver.load();
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD)) {
            PreparedStatement preparedStatement1 = connection.prepareStatement("INSERT INTO STUDENT VALUES(?,?,?)");
            preparedStatement1.setInt(1, id++);
            preparedStatement1.setString(2, name);
            preparedStatement1.setString(3, value);
            preparedStatement1.executeUpdate();

        } catch (SQLException e) {
            logger.logError(e, "Data not valid");
            throw new IllegalArgumentException("Data not valid ", e);
        }
    }

    @Override
    public void delete(String name) {
        Driver.load();
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD)) {
            PreparedStatement preparedStatement1 = connection.prepareStatement("delete from STUDENT where FIRST_NAME = ?");
            preparedStatement1.setString(1, name);
            preparedStatement1.execute();

        } catch (SQLException e) {
            logger.logError(e, "Data not valid");
            throw new IllegalArgumentException("Data not valid ", e);
        }

    }

    @Override
    public void invalidate() {

    }

    @Override
    public List<Student> getData() {
        Driver.load();
        List<Student> students = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD)) {
            PreparedStatement selectPreparedStatement = connection.prepareStatement("select * from STUDENT");
            ResultSet rs = selectPreparedStatement.executeQuery();

            while (rs.next()) {
                students.add(new Student(rs.getInt("ID"), rs.getString("FIRST_NAME"), rs.getString("LAST_NAME")));
            }
        } catch (SQLException e) {
            logger.logError(e, "Data not valid");
            throw new IllegalArgumentException("Data not valid ", e);
        }
        return students;
    }

}
