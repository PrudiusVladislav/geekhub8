package org.geekhub.lesson14.task1.view;

import org.geekhub.lesson14.task1.controller.Action;
import org.geekhub.lesson14.task1.controller.ActionType;
import org.geekhub.lesson14.task1.model.DataBaseSource;
import org.geekhub.lesson14.task1.model.SessionSource;
import org.geekhub.lesson14.task1.model.Source;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static java.util.Objects.nonNull;
import static org.geekhub.lesson14.task1.controller.ActionFactory.createAction;

@WebServlet(name = "registration", urlPatterns = "/registration")
public class Registration extends HttpServlet {

    final static Source source = new DataBaseSource();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final HttpSession session = req.getSession();
        String header = req.getHeader("user-agent");

        if (nonNull(req.getQueryString())) {
            String actionFromClient = req.getParameter("action");
            String name = req.getParameter("name");
            String value = req.getParameter("value");
            ActionType actionType = ActionType.recognize(actionFromClient);
            Action action = createAction(actionType);

            if (header.contains("Edge")) {
                Source sessionSource = getSession(session);
                action.apply(sessionSource, name, value);
            } else {
                action.apply(source, name, value);
                session.setAttribute("students", source.getData());
            }
        }

        req.getRequestDispatcher("WEB-INF/table.jsp").forward(req, resp);
    }

    private static Source getSession(HttpSession session) {
        return new SessionSource(session);
    }
}