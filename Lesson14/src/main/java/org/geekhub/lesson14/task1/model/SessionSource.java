package org.geekhub.lesson14.task1.model;

import org.geekhub.lesson14.task1.Student;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class SessionSource implements Source {
    private final HttpSession session;
    private int id = 1;
    private static List<Student> students = new ArrayList<>();

    public SessionSource(HttpSession session) {
        this.session = session;
    }

    @Override
    public void put(String name, String value) {
        students.add(new Student(id, name, value));
        session.setAttribute("students", students);
    }

    @Override
    public void delete(String name) {
        students = students.stream()
                .filter(student -> !student.getFirstName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public void invalidate() {
        session.invalidate();
    }

    @Override
    public List<Student> getData() {
        return (List<Student>) session.getAttribute("students");
    }
}
