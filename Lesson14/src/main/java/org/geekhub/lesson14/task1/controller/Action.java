package org.geekhub.lesson14.task1.controller;

import org.geekhub.lesson14.task1.model.Source;

public interface Action {
    void apply(Source source, String name, String value);
}
