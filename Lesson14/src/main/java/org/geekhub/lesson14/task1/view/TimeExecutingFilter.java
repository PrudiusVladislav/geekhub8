package org.geekhub.lesson14.task1.view;

import org.geekhub.lesson10.logger.ConsoleLogger;
import org.geekhub.lesson10.logger.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.time.LocalTime;

@WebFilter(filterName = "timeExecutionFilter")
public class TimeExecutingFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Logger logger = new ConsoleLogger(TimeExecutingFilter.class);
        LocalTime beforeFilter = LocalTime.now();
        logger.logInfo("before filter - " + beforeFilter);
        try {
            chain.doFilter(request, response);
        } finally {
            LocalTime afterFilter = LocalTime.now();
            logger.logInfo("after filter - " + (afterFilter.toNanoOfDay() - beforeFilter.toNanoOfDay()));
        }
    }
}