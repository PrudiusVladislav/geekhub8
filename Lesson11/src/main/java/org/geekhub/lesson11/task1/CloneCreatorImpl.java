package org.geekhub.lesson11.task1;

import org.geekhub.lesson11.task1.exception.CanNotCloneException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;

public class CloneCreatorImpl implements CloneCreator {
    @Override
    public <T extends CanBeCloned> T clone(T object) throws CanNotCloneException {
        Class clazz = object.getClass();
        T cloneObj = null;
        try {
            cloneObj = (T) clazz.getConstructor().newInstance();
            Field[] declaredFields = clazz.getDeclaredFields();
            Type[] genericInterfaces = clazz.getGenericInterfaces();

            for (Type inter : genericInterfaces) {
                if (!inter.getTypeName().contains("CanBeCloned")) {
                    throw new CanNotCloneException();
                }
            }

            for (Field field : declaredFields) {
                field.setAccessible(true);
                if (!field.isAnnotationPresent(Ignore.class)) {
                    if (isSimpleType(field.getType())) {
                        field.set(cloneObj, field.get(object));
                    } else {
                        field.set(cloneObj, clone((T) field.get(object)));
                    }
                }
            }

        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new CanNotCloneException();
        }

        return cloneObj;
    }

    private static boolean isSimpleType(Class<?> type) {
        return type.isPrimitive() || type == String.class
                || type == Double.class || type == Float.class
                || type == Long.class || type == Integer.class
                || type == Short.class || type == Character.class
                || type == Byte.class || type == Boolean.class;
    }
}