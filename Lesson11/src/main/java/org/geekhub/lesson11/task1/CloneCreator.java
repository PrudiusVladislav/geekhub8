package org.geekhub.lesson11.task1;

import org.geekhub.lesson11.task1.exception.CanNotCloneException;

public interface CloneCreator {
    <T extends CanBeCloned> T clone(T object) throws CanNotCloneException;
}