package org.geekhub.lesson11.task2;

import java.lang.reflect.Field;
import java.util.Objects;

public class ChangeSetExtractorImpl<T> implements ChangeSetExtractor<T> {
    @Override
    public ChangeSet extract(T oldObject, T newObject) {
        Class<?> oldClazz = oldObject.getClass();
        Class<?> newClazz = newObject.getClass();
        ChangeSet changeSet = new ChangeSet();

        Field[] declaredOldFields = oldClazz.getDeclaredFields();
        Field[] declaredNewFields = newClazz.getDeclaredFields();

        try {
            for(Field fieldOld : declaredOldFields) {
                    for(Field fieldNew : declaredNewFields) {
                        fieldOld.setAccessible(true);
                        fieldNew.setAccessible(true);
                        if(Objects.isNull(fieldOld.getDeclaredAnnotation(Ignore.class) )) {
                        if (fieldOld.getName().equals(fieldNew.getName())) {
                            if (!fieldNew.get(oldObject).equals(fieldOld.get(newObject))) {
                                changeSet.addChangeElement(
                                        fieldOld.getName(),
                                        fieldOld.get(oldObject),
                                        fieldNew.get(newObject)
                                );
                            }
                        }
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return changeSet;
    }
}