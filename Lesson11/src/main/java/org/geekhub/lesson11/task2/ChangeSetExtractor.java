package org.geekhub.lesson11.task2;

public interface ChangeSetExtractor<T> {
    ChangeSet extract(T oldObject, T newObject);
}
