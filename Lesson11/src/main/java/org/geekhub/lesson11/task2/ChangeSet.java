package org.geekhub.lesson11.task2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ChangeSet {
    private Map<String, List<Object>> map = new HashMap<>();

    void addChangeElement(String fieldName, Object oldValue, Object newValue) {
        List<Object> list = new ArrayList<>();
        list.add(oldValue);
        list.add(newValue);
        map.put(fieldName, list);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChangeSet changeSet = (ChangeSet) o;
        return Objects.equals(map, changeSet.map);
    }

    @Override
    public int hashCode() {
        return Objects.hash(map);
    }
}
