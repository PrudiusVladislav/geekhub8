package org.geekhub.lesson11.task2;

import java.util.Objects;

public class Tree {
    String color;
    int length;
    boolean withLeaf;

    public Tree(String color, int length, boolean withLeaf) {
        this.color = color;
        this.length = length;
        this.withLeaf = withLeaf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tree tree = (Tree) o;
        return length == tree.length &&
                withLeaf == tree.withLeaf &&
                Objects.equals(color, tree.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, length, withLeaf);
    }
}
