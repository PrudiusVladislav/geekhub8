package org.geekhub.lesson11.task2;

import java.util.Objects;

public class User {
    private int id;
    @Ignore
    private String name;
    private boolean work;

    public User(int id, String name, boolean work) {
        this.id = id;
        this.name = name;
        this.work = work;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                work == user.work &&
                Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, work);
    }
}
