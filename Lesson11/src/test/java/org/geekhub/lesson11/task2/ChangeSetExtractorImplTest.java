package org.geekhub.lesson11.task2;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

public class ChangeSetExtractorImplTest {

    @Test
    public void shouldNotCompareFieldWithAnnotationIgnore() {
        ChangeSetExtractor changeSetExtractor = new ChangeSetExtractorImpl();
        User user1 = new User(1, "Вася", true);
        User user2 = new User(2, "Федя", true);

        ChangeSet expected = new ChangeSet();
        expected.addChangeElement("id", 1, 2);

        ChangeSet actual = changeSetExtractor.extract(user1, user2);

        Assert.assertEquals(actual, expected);

    }

    @Test
    public void shouldCompareFieldsWithoutAnnotationIgnore() {
        ChangeSetExtractor changeSetExtractor = new ChangeSetExtractorImpl();
        Tree tree1 = new Tree("green", 24, true);
        Tree tree2 = new Tree("green", 5, false);

        ChangeSet expected = new ChangeSet();
        expected.addChangeElement("length", 24, 5);
        expected.addChangeElement("withLeaf", true, false);

        ChangeSet actual = changeSetExtractor.extract(tree1, tree2);

        Assert.assertEquals(actual, expected);
    }
}