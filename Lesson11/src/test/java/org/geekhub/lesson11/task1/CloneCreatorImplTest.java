package org.geekhub.lesson11.task1;

import org.geekhub.lesson11.task1.exception.CanNotCloneException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CloneCreatorImplTest {

    @Test
    public void shouldNotCloneFieldWithAnnotationIgnore() throws CanNotCloneException {
        CloneCreator cloneCreator = new CloneCreatorImpl();
        User user = new User(1, 25, "Васе");
        User actual = cloneCreator.clone(user);

        Assert.assertNotEquals(actual, user);
    }

    @Test
    public void shouldCloneFieldWithoutAnnotationIgnore() throws CanNotCloneException {
        CloneCreator cloneCreator = new CloneCreatorImpl();
        Dog dog = new Dog(13, "White", true, new User(1, 25, "Васе"));

        Dog actual = cloneCreator.clone(dog);
        Dog expected = new Dog(13, "White", true, new User(1, 0, "Васе"));

        Assert.assertEquals(actual, expected);
    }

}