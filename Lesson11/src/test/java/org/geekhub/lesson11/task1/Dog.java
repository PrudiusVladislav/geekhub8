package org.geekhub.lesson11.task1;

import java.util.Objects;

public class Dog implements CanBeCloned {
    private int age;
    private String color;
    private boolean hasVoice;
    private User master;

    public Dog() {
    }

    public Dog(int age, String color, boolean hasVoice, User master) {
        this.age = age;
        this.color = color;
        this.hasVoice = hasVoice;
        this.master = master;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return age == dog.age &&
                hasVoice == dog.hasVoice &&
                Objects.equals(color, dog.color) &&
                master.equals(dog.master);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, color, hasVoice, master);
    }
}
