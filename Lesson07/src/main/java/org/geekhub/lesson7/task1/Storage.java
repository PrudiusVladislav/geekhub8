package org.geekhub.lesson7.task1;

import java.util.*;

public class Storage {
    private List<User> users = new ArrayList<>();

    int addUserToStorage(User user) {
        users.add(user);
        return users.size();
    }

    Optional<User> getUserByLogin(String login) {
        for (User user : users) {
            if(user.getLogin().equals(login)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    List<User> getSortUser() {
        List<User> users = new ArrayList<>(this.users);
        Collections.sort(users);
        return users;

    }

    boolean updateUser(int id, User user) {
        users.set(id, user);
        return users.contains(user);
    }

    public boolean removeUser(String login) {
        return users.remove(login);
    }
}
