package org.geekhub.lesson7.task1;

import org.geekhub.lesson7.task1.exception.UserNotFoundException;

import java.util.*;

public class UserServiceImpl implements UserService {
    private Storage storage = new Storage();

    @Override
    public int save(User user) {

        if (Objects.isNull(user)) {
           throw new IllegalArgumentException();
        }

        if (user.getLogin().isEmpty()) {
            throw new IllegalArgumentException();
        }

        if (user.getLogin().equals("admin")) {
            throw new IllegalArgumentException();
        }

        user.setId(storage.addUserToStorage(user));

        return 0;
    }

    @Override
    public User getUser(String login) {
       Optional<User> user = storage.getUserByLogin(login);

        if(user.isPresent()) {
            throw new UserNotFoundException();
        }

        return  user.get();
    }

    @Override
    public List<User> getAllUser() {
        List<User> user = storage.getSortUser();
        return user;
    }

    @Override
    public boolean removeUser(String login) {
        return storage.removeUser(login);
    }

    @Override
    public void updateUser(int id, User user) {
        storage.updateUser(id, user);
    }
}
