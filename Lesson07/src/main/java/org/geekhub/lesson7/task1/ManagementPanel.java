package org.geekhub.lesson7.task1;

import java.util.List;

public class ManagementPanel {
    private UserService userService = new UserServiceImpl();

    void createUser(String login, String password, String fullName) {
        userService.save(new User(login, password, fullName));
    }

    void printAllUsers() {
        List<User> users = userService.getAllUser();

        users.forEach(System.out::println);
    }

    void updateUserById(int id, User user) {
        userService.updateUser(id, user);

    }

    public boolean removeUser(String login) {
        return userService.removeUser(login);
    }
}