package org.geekhub.lesson7.task1;

import java.time.LocalDate;

public class License {
    LocalDate start;
    LocalDate end;
    Type type;

    public License(LocalDate start, LocalDate end, Type type) {
        this.start = start;
        this.end = end;
        this.type = type;
    }

    public LocalDate getStart() {
        return start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public Type getType() {
        return type;
    }
}
