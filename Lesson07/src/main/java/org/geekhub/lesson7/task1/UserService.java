package org.geekhub.lesson7.task1;

import java.util.List;
import java.util.Optional;

public interface UserService {
    int save(User user);

    User getUser(String login);

    List<User> getAllUser();

    boolean removeUser(String login);

    void updateUser(int id, User user);
}
