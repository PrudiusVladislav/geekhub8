package org.geekhub.lesson7.task1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static Scanner scan = new Scanner(System.in);
    static ManagementPanel managementPanel = new ManagementPanel();
    public static void main(String[] args) {
        String login;
        String password;

        System.out.println("Write user login: ");
        login = scan.next();
        System.out.println("Write user password: ");
        password = scan.next();

        if(login.equals("admin") && password.equals("admin")) {
            while (true) {
                System.out.println("Choose command : ");
                System.out.println("1 - Create new user");
                System.out.println("2 - Print all users");
                System.out.println("3 - Update user by id");
                System.out.println("4 - Remove user by login");

                int command = scan.nextInt();
                switch (command) {
                    case 1:
                        createUser();
                        break;
                    case 2:
                        printAllUsers();
                        break;
                    case 3:
                        updateUserById();
                        break;
                    case 4:
                        removeUserByLogin();
                }

            }
        }
    }

    private static void removeUserByLogin() {
        System.out.println("Write user login: ");
        String login = scan.next();
        managementPanel.removeUser(login);
    }

    private static void updateUserById() {
        System.out.println("Write user id: ");
        int id = scan.nextInt();
        System.out.println("Write user login");
        String login = scan.next();
        System.out.println("Write user password");
        String password = scan.next();
        System.out.println("Write user full name");
        String fullName = scan.next();

        managementPanel.updateUserById(id, new User(login, password, fullName));


    }

    private static void printAllUsers() {
        managementPanel.printAllUsers();
    }

    private static void createUser() {
        System.out.println("Write user login");
        String login = scan.next();
        System.out.println("Write user password");
        String password = scan.next();
        System.out.println("Write user full name");
        String fullName = scan.next();

        System.out.println("Add license? 1\0");
        if(scan.nextBoolean()) {
            List<License> licenses = new ArrayList<>();
            System.out.println("Write start license ");
            String start = scan.next();
            System.out.println("Write end license ");
            String end = scan.next();
            licenses.add(new License(LocalDate.parse(start), LocalDate.parse(end), Type.FULL));
        }

        managementPanel.createUser(login, password, fullName);

    }
}