package org.geekhub.lesson7.task1;

import java.util.List;

public class User implements Comparable<User>{

    private int id;
    private String login;
    private String password;
    private String fullName;
    private boolean admin;
    private List<License> licenses;


    public User(String login,String password, String fullName) {
        this.login = login;
        this.fullName = fullName;
        this.password = password;
    }

    public void setLicenses(List<License> licenses) {
        this.licenses = licenses;
    }

    public String getLogin() {
        return login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<License> getLicenses() {
        return licenses;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public int compareTo(User o) {
        if (this.getFullName().equals(o.getFullName())) {
            return this.getLogin().compareTo(o.getLogin());
        }
        return getFullName().compareTo(o.getFullName());
    }

}
