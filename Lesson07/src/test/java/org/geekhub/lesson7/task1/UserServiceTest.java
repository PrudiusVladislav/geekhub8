package org.geekhub.lesson7.task1;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class UserServiceTest {
    private UserService userService;
    private List<License> licenses;

    @BeforeMethod
    public void setUp() {
        userService = new UserServiceImpl();
        licenses = new ArrayList();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldNotSaveNotNull() {
        userService.save(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldNotSaveWithoutLogin() {
        userService.save(new User("", "", ""));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldNotSaveAdmin() {
        userService.save(new User("admin", "admin", ""));
    }

    @Test(expectedExceptions = NoSuchElementException.class)
    public void shouldNodGetUserWithoutLogin () {
        userService.save(new User("user1", "admin", ""));

        userService.getUser("");

    }

    @Test
    public void shouldNotReturnSortedUser() {
        userService.save(new User("user1", "admin1", ""));
        userService.save(new User("user5", "admin7", ""));
        userService.save(new User("user6", "admin0", ""));
        userService.save(new User("user1", "admin0", ""));
        userService.save(new User("user9", "admin2", ""));

        List<User> actual = userService.getAllUser();

        Assert.assertFalse(notSortedUser().equals(actual));
    }


    private List<User> notSortedUser() {
        List<User> user = new ArrayList<>();
        user.add(new User("user1", "admin1", ""));
        user.add(new User("user5", "admin7", ""));
        user.add(new User("user6", "admin0", ""));
        user.add(new User("user1", "admin0", ""));
        user.add(new User("user9", "admin2", ""));
        return user;
    }

    private List<User> sortedUser() {
        List<User> user = new ArrayList<>();
        user.add(new User("user1", "admin0", ""));
        user.add(new User("user6", "admin0", ""));
        user.add(new User("user1", "admin1", ""));
        user.add(new User("user9", "admin2", ""));
        user.add(new User("user5", "admin7", ""));
        return user;
    }

}