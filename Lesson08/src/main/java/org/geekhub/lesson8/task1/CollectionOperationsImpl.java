package org.geekhub.lesson8.task1;

import java.util.*;
import java.util.function.*;

public class CollectionOperationsImpl implements CollectionOperations {

    @Override
    public <E> List<E> fill(Supplier<E> producer, int count) {
        List<E> producers = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            producers.add(producer.get());
        }
        return producers;
    }

    @Override
    public <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> result = new ArrayList<>();
        for(E elem : elements){
            if(filter.test(elem)) {
                result.add(elem);
            }
        }
        return result;
    }

    @Override
    public <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if(predicate.test(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if(!predicate.test(element)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        return !anyMatch(elements, predicate);
    }

    @Override
    public <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        List<R> list = new ArrayList<>();

        for(T element : elements) {
            list.add(mappingFunction.apply(element));
        }

        return list;
    }

    @Override
    public <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }

        E max = elements.get(0);

        for (E element : elements) {
            if (comparator.compare(element, max) > 0) {
                max = element;
            }
        }
        return Optional.of(max);
    }

    @Override
    public <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }

        E min = elements.get(0);

        for (E element : elements) {
            if (comparator.compare(element, min) < 0) {
                min = element;
            }
        }
        return Optional.of(min);
    }

    @Override
    public <E> List<E> distinct(List<E> elements) {
        List<E> uniqueElements = new ArrayList<>();

        if(Objects.isNull(elements)) {
            return uniqueElements;
        }

        for(E element : elements) {
            if(!uniqueElements.contains(element)) {
                uniqueElements.add(element);
            }
        }
        return uniqueElements;
    }

    @Override
    public <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for(E element : elements) {
            consumer.accept(element);
        }
    }

    @Override
    public <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        if(elements.isEmpty()) {
            return Optional.empty();
        }

        E element = elements.get(0);

        if(elements.size() == 1) {
            return Optional.of(element);
        }

        for (int i = 1; i < elements.size(); i++) {
            element = accumulator.apply(element, elements.get(i));
        }

        return Optional.of(element);
    }

    @Override
    public <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E element = seed;

        if(elements.isEmpty()) {
            return seed;
        }

        for(E elem : elements) {
            element = accumulator.apply(element, elem);
        }

        return element;
    }

    @Override
    public <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        List<E> good = new ArrayList<>();
        List<E> bad = new ArrayList<>();
        Map<Boolean, List<E>> map = new HashMap<>();

        if(elements.isEmpty()) {
            return Collections.emptyMap();
        }

        for(E element : elements) {
            if(predicate.test(element)) {
                good.add(element);
            } else {
                bad.add(element);
            }
        }

        map.put(true, good);
        map.put(false, bad);

        return map;
    }

    @Override
    public <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        Map<K, List<T>> map = new HashMap<>();
        List<T> result;
        List<T> newResult;

        for(T element : elements) {
            K key = classifier.apply(element);
            if(map.containsKey(key)) {
                result = map.get(key);
                result.add(element);
            } else {
                newResult = new ArrayList<>();
                newResult.add(element);
                map.put(key, newResult);
            }
        }
        return map;
    }

    @Override
    public <T, K, U> Map<K, U> toMap(List<T> elements, Function<T, K> keyFunction, Function<T, U> valueFunction, BinaryOperator<U> mergeFunction) {
        Map<K, U> map = new HashMap<>();
        K key;
        U value;

        for (T element : elements) {
            key = keyFunction.apply(element);
            value = valueFunction.apply(element);

            if(map.containsValue(value)) {
               map.replace(key, mergeFunction.apply(value, map.get(value)));
            }
            map.put(key, value);
        }

        return map;
    }
}