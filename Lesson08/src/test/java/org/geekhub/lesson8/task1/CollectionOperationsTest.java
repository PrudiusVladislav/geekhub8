package org.geekhub.lesson8.task1;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;
import java.util.function.*;

public class CollectionOperationsTest {
    private CollectionOperations collectionOperations;
    private List<User> users;


    @BeforeMethod
    public void setUp() {
        collectionOperations = new CollectionOperationsImpl();
        users = new ArrayList<>();
        users.add(new User(1, "User1", true));
        users.add(new User(2, "User1", false));
        users.add(new User(3, "User3", false));
        users.add(new User(4, "User4", true));
    }

    @Test
    public void shouldAddElementWithAnonymousClass() {
        Supplier<User> supplier = new Supplier<User>() {
            @Override
            public User get() {
                return getUser();
            }
        };

        List<User> actual = collectionOperations.fill(supplier, 6);

        Assert.assertEquals(actual.size(), 6);
    }

    @Test
    public void shouldAddElementWithLambda() {
        Supplier<User> supplier = CollectionOperationsTest::getUser;

        List<User> actual = collectionOperations.fill(supplier, 6);

        Assert.assertEquals(actual.size(), 6);
    }


    @Test
    public void shouldAddElementWithMethodReference() {
        Supplier<User> supplier = CollectionOperationsTest::getUser;

        List<User> actual = collectionOperations.fill(supplier, 6);
        Assert.assertEquals(actual.size(), 6);
    }

    @Test
    public void shouldFilterListWithAnonymousClass() {
        Predicate<User> predicate = new Predicate<User>() {
            @Override
            public boolean test(User user) {
                return user.isAdmin();
            }
        };

        List<User> actual = collectionOperations.filter(users, predicate);

        Assert.assertEquals(actual.size(), 2);
    }

    @Test
    public void shouldFilterListWithLambda() {
        Predicate<User> predicate = user -> user.isAdmin();

        List<User> actual = collectionOperations.filter(users, predicate);

        Assert.assertEquals(actual.size(), 2);
    }

    @Test
    public void shouldFilterListWithMethodReference() {
        Predicate<User> predicate = User::isAdmin;

        List<User> actual = collectionOperations.filter(users, predicate);

        Assert.assertEquals(actual.size(), 2);
    }

    @Test
    public void shouldAnyMatchListWithAnonymousClass() {
        Predicate<User> predicate = new Predicate<User>() {
            @Override
            public boolean test(User user) {
                return user.isAdmin();
            }
        };

        Assert.assertTrue(collectionOperations.anyMatch(users, predicate));
    }

    @Test
    public void shouldAnyMatchListWithLambda() {
        Predicate<User> predicate = user -> user.isAdmin();

        Assert.assertTrue(collectionOperations.anyMatch(users, predicate));
    }

    @Test
    public void shouldAnyMatchListWithMethodReference() {
        Predicate<User> predicate = User::isAdmin;

        Assert.assertTrue(collectionOperations.anyMatch(users, predicate));
    }

    @Test
    public void shouldAllMatchListWithAnonymousClass() {
        Predicate<User> predicate = new Predicate<User>() {
            @Override
            public boolean test(User user) {
                return user.isAdmin();
            }
        };

        Assert.assertFalse(collectionOperations.allMatch(users, predicate));
    }

    @Test
    public void shouldAllMatchListWithLambda() {
        Predicate<User> predicate = user -> user.isAdmin();

        Assert.assertFalse(collectionOperations.allMatch(users, predicate));
    }

    @Test
    public void shouldAllMatchListWithMethodReference() {
        Predicate<User> predicate = User::isAdmin;

        Assert.assertFalse(collectionOperations.allMatch(users, predicate));
    }

    @Test
    public void shouldNoneMatchListWithAnonymousClass() {
        Predicate<User> predicate = new Predicate<User>() {
            @Override
            public boolean test(User user) {
                return user.isAdmin();
            }
        };

        Assert.assertFalse(collectionOperations.noneMatch(users, predicate));
    }

    @Test
    public void shouldNoneMatchListWithLambda() {
        Predicate<User> predicate = user -> user.isAdmin();

        Assert.assertFalse(collectionOperations.noneMatch(users, predicate));
    }

    @Test
    public void shouldNoneMatchListWithMethodReference() {
        Predicate<User> predicate = User::isAdmin;

        Assert.assertFalse(collectionOperations.noneMatch(users, predicate));
    }

    @Test
    public void shouldMapListWithAnonymousClass() {
        Function<User, Integer> function = new Function<User, Integer>() {
            @Override
            public Integer apply(User user) {
                return user.getId();
            }
        };
        List<Integer> actual = collectionOperations.map(users, function);

        Assert.assertEquals(actual.size(), users.size());
    }

    @Test
    public void shouldMapListWithLambda() {
        Function<User, Integer> function = user -> user.getId();

        List<Integer> actual = collectionOperations.map(users, function);

        Assert.assertEquals(actual.size(), users.size());
    }

    @Test
    public void shouldMapListWithMethodReference() {
        Function<User, Integer> function = User::getId;

        List<Integer> actual = collectionOperations.map(users, function);

        Assert.assertEquals(actual.size(), users.size());
    }

    @Test
    public void shouldFoundMaxElementInListWithAnonymousClass() {
        Comparator<User> comparator = new Comparator<User>() {
            @Override
            public int compare(User user1, User user2) {
                return user1.compareTo(user2);
            }
        };

        Optional<User> user = collectionOperations.max(users, comparator);

        Assert.assertTrue(user.isPresent());
        Assert.assertEquals(user.get().getId(), 4);

    }

    @Test
    public void shouldFoundMaxElementInListWithLambda() {
        Comparator<User> comparator = (user1, user2) -> user1.compareTo(user2);

        Optional<User> user = collectionOperations.max(users, comparator);

        Assert.assertTrue(user.isPresent());
        Assert.assertEquals(user.get().getId(), 4);

    }

    @Test
    public void shouldFoundMaxElementInListWithMethodReference() {
        Comparator<User> comparator = User::compareTo;

        Optional<User> user = collectionOperations.max(users, comparator);

        Assert.assertTrue(user.isPresent());
        Assert.assertEquals(user.get().getId(), 4);

    }

    @Test
    public void shouldFoundMinElementInListWithAnonymousClass() {
        Comparator<User> comparator = new Comparator<User>() {
            @Override
            public int compare(User user1, User user2) {
                return user1.compareTo(user2);
            }
        };

        Optional<User> user = collectionOperations.min(users, comparator);

        Assert.assertTrue(user.isPresent());
        Assert.assertEquals(user.get().getId(), 1);

    }

    @Test
    public void shouldFoundMinElementInListWithLambda() {
        Comparator<User> comparator = (user1, user2) -> user1.compareTo(user2);

        Optional<User> user = collectionOperations.min(users, comparator);

        Assert.assertTrue(user.isPresent());
        Assert.assertEquals(user.get().getId(), 1);

    }

    @Test
    public void shouldFoundMinElementInListWithMethodReference() {
        Comparator<User> comparator = User::compareTo;

        Optional<User> user = collectionOperations.min(users, comparator);

        Assert.assertTrue(user.isPresent());
        Assert.assertEquals(user.get().getId(), 1);

    }

    @Test
    public void shouldReturnListOfUniqueElements() {
        List<User> uniqueElements = collectionOperations.distinct(users);

        Assert.assertEquals(uniqueElements.size(), 4);
    }

    @Test
    public void shouldReturnEmptyListOfUniqueElements() {
        List<User> uniqueElements = collectionOperations.distinct(null);

        Assert.assertTrue(uniqueElements.isEmpty());
    }

    @Test
    public void shouldReturnEmptyListOfUniqueElementsWithEmptyList() {
        List<User> uniqueElements = collectionOperations.distinct(new ArrayList<>());

        Assert.assertTrue(uniqueElements.isEmpty());
    }

    @Test
    public void shouldAddUserInLocalListWithAnonymousClass() {
        List<User> actualUsers = new ArrayList<>();
        Consumer<User> consumer = new Consumer<User>() {
            @Override
            public void accept(User user) {
                actualUsers.add(user);
            }
        };

        collectionOperations.forEach(users, consumer);

        Assert.assertEquals(actualUsers.size(), users.size());

    }

    @Test
    public void shouldAddUserInLocalListWithLambda() {
        List<User> actualUsers = new ArrayList<>();
        Consumer<User> consumer = user -> actualUsers.add(user);

        collectionOperations.forEach(users, consumer);

        Assert.assertEquals(actualUsers.size(), users.size());

    }

    @Test
    public void shouldAddUserInLocalListWithMethodReference() {
        List<User> actualUsers = new ArrayList<>();
        Consumer<User> consumer = actualUsers::add;

        collectionOperations.forEach(users, consumer);

        Assert.assertEquals(actualUsers.size(), users.size());

    }

    @Test
    public void shouldCalculatedNumberWithAnonymousClass() {
        BinaryOperator<Integer> binaryOperator = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer number1, Integer number2) {
                return number1 + number2;
            }
        };

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        Optional<Integer> actual = collectionOperations.reduce(list, binaryOperator);
        Integer expected = 15;

        Assert.assertTrue(actual.isPresent());
        Assert.assertEquals(actual.get(), expected);
    }

    @Test
    public void shouldReturnNumberWhenListSize_1() {
        BinaryOperator<Integer> binaryOperator = (number1, number2) -> number1 + number2;

        List<Integer> list = new ArrayList<>();
        list.add(1);

        Optional<Integer> actual = collectionOperations.reduce(list, binaryOperator);
        Integer expected = 1;

        Assert.assertTrue(actual.isPresent());
        Assert.assertEquals(actual.get(), expected);
    }

    @Test
    public void shouldReturnOptionalEmptyWhenListSize_0() {
        BinaryOperator<Integer> binaryOperator = CollectionOperationsTest::apply;

        Optional<Integer> actual = collectionOperations.reduce(Collections.emptyList(), binaryOperator);

        Assert.assertTrue(actual.isEmpty());
    }


    @Test
    public void shouldAccumulateNumberWhenSeed_0() {
        BinaryOperator<Integer> binaryOperator = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer number1, Integer number2) {
                return number1 + number2;
            }
        };

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        Integer actual = collectionOperations.reduce(0 ,list, binaryOperator);
        Integer expected = 6;

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldReturnSeedWhenListIsEmpty() {
        BinaryOperator<Integer> binaryOperator = (number1, number2) -> number1 + number2;

        Integer seed = 0;
        Integer actual = collectionOperations.reduce(seed ,Collections.emptyList(), binaryOperator);
        Integer expected = 0;

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldAccumulateNumberWhenSeed_7() {
        BinaryOperator<Integer> binaryOperator = CollectionOperationsTest::apply;

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        Integer seed = 7;
        Integer actual = collectionOperations.reduce(seed ,list, binaryOperator);
        Integer expected = 13;

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldReturnEmptyMapWhenInputEmptyList() {
        Predicate<User> predicate = new Predicate<User>() {
            @Override
            public boolean test(User user) {
                return user.getId() < 2;
            }
        };

        Map<Boolean, List<User>> actual = collectionOperations.partitionBy(Collections.emptyList(), predicate);

        Assert.assertTrue(actual.isEmpty());
    }

    @Test
    public void shouldSplitElementIntoTwoGroups() {
        Predicate<User> predicate = user -> user.getId() < 2;

        List<User> expectedUsersWherePredicateTrue = new ArrayList<>();
        List<User> expectedUsersWherePredicateFalse = new ArrayList<>();
        expectedUsersWherePredicateTrue.add(new User(1, "User1", true));
        expectedUsersWherePredicateFalse.add(new User(2, "User2", false));
        expectedUsersWherePredicateFalse.add(new User(3, "User3", false));
        expectedUsersWherePredicateFalse.add(new User(4, "User4", true));

        Map<Boolean, List<User>> actual = collectionOperations.partitionBy(users, predicate);
        Map<Boolean, List<User>> expected = new HashMap<>();
        expected.put(true, expectedUsersWherePredicateTrue);
        expected.put(false, expectedUsersWherePredicateFalse);

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldGroupUserByAdmin() {
        Function<User, Boolean> function = new Function<User, Boolean>() {
            @Override
            public Boolean apply(User user) {
                return user.isAdmin();
            }
        };

        List<User> expectedAdmins = new ArrayList<>();
        List<User> expectedNotAdmin = new ArrayList<>();

        expectedAdmins.add(new User(1, "User1", true));
        expectedAdmins.add(new User(4, "User4", true));
        expectedNotAdmin.add(new User(2, "User2", false));
        expectedNotAdmin.add(new User(3, "User3", false));

        Map<Boolean, List<User>> expected = new HashMap<>();
        expected.put(true, expectedAdmins);
        expected.put(false, expectedNotAdmin);

        Map<Boolean, List<User>> actual = collectionOperations.groupBy(users, function);

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldCreateMapUsers() {
       Function<User, Integer> key = new Function<User, Integer>() {
           @Override
           public Integer apply(User user) {
               return user.getId();
           }
       };

       Function<User, String> value = user -> user.getName();

        BinaryOperator<String> merge = (s, s2) -> s + s2;

        Map<Integer, String> actual = collectionOperations.toMap(users, key, value, merge);
        int countKey = actual.keySet().size();

        Assert.assertEquals(countKey, 4);
    }


    private static User getUser() {
        return new User(1, "user1", true);
    }

    private static Integer apply(Integer number1, Integer number2) {
        return number1 + number2;
    }
}