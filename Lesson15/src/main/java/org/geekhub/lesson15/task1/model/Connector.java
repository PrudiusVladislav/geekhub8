package org.geekhub.lesson15.task1.model;

import org.geekhub.lesson10.logger.ConsoleLogger;
import org.geekhub.lesson10.logger.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
@PropertySource("config.properties")
public class Connector {
    private Logger logger = new ConsoleLogger(DataBaseSource.class);
    private Connection connection;

    @Autowired
    private Environment env;

        @Bean
        public Connection createConnection() {
        String url = env.getProperty("db.host");
        String login = env.getProperty("db.login");
        String password = env.getProperty("db.password");

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, login, password);
        } catch (SQLException | ClassNotFoundException e) {
            logger.logError(e, "Dont create connection to DB: ");
            throw new IllegalArgumentException("Dont create connection to DB: ", e);
        }
            return connection;
    }
}