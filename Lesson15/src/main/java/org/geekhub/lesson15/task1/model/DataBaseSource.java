package org.geekhub.lesson15.task1.model;

import org.geekhub.lesson10.logger.ConsoleLogger;
import org.geekhub.lesson10.logger.Logger;
import org.geekhub.lesson15.task1.User;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class DataBaseSource implements Source {
    Logger logger = new ConsoleLogger(DataBaseSource.class);
    Connection connection;

    public DataBaseSource() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Connector.class);
        this.connection = context.getBean(Connector.class).createConnection();
        createDataBase();
        createTable();
    }

    public void createTable() {

        try {
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT exists STUDENT(ID SERIAL PRIMARY KEY, LOGIN VARCHAR(15), PASSWORD VARCHAR(20), FULL_NAME VARCHAR(20), SECOND_NAME VARCHAR(20), ADMIN BOOLEAN)");
            statement.executeUpdate("INSERT INTO STUDENT(LOGIN, PASSWORD, FULL_NAME, SECOND_NAME, ADMIN) values('admin', 'admin', 'admin', 'admin', true)");
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.logError(e, "Data Base dont create");
            throw new IllegalArgumentException("Data Base dont create: ", e);
        }
    }

    private void createDataBase() {
        try {
            if (!isDataBaseExists()) {
                PreparedStatement ps = connection.prepareStatement("CREATE DATABASE VladislavPrudius");
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            logger.logError(e, "Data Base not created: ");
            throw new IllegalArgumentException("Data Base not created: ", e);
        }
    }

    private boolean isDataBaseExists() {
        try {
            PreparedStatement ps = connection.prepareStatement("select * from pg_database");
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString(1).equals("vladislavprudius")) {
                    return true;
                }
            }

        } catch (SQLException e) {
            logger.logError(e, "Data Base not created: ");
            throw new IllegalArgumentException("Data Base not created: ", e);
        }
        return false;
    }

    @Override
    public boolean contains(String login, String password) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM STUDENT where login=? AND password=?");
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                return true;
            }

        } catch (SQLException e) {
            logger.logError(e, "Data Base not contains: " + login);
            throw new IllegalArgumentException("Data Base not contains: " + login, e);
        }
        return false;
    }

    @Override
    public boolean isAdmin(String login, String password) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT admin FROM student where login=? and password=?");
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
               return rs.getBoolean("admin");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean add(String login, String password, String firstName, String lastName, Boolean isAdmin) {
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO STUDENT(LOGIN, PASSWORD, FULL_NAME, SECOND_NAME, ADMIN) VALUES (?,?,?,?,?)");
            ps.setString(1, login);
            ps.setString(2, password);
            ps.setString(3, firstName);
            ps.setString(4, lastName);
            ps.setBoolean(5, isAdmin);

            ps.execute();
            return true;
        } catch (SQLException e) {
            logger.logError(e, "Failed add data to DB");
            throw new IllegalArgumentException("Failed add data to DB: ", e);
        }
    }

    @Override
    public List<User> getAllUser() {
        List<User> users = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM STUDENT");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                users.add(new User(
                        rs.getInt("id"),
                        rs.getString("login"),
                        rs.getString("password"),
                        rs.getString("full_name"),
                        rs.getString("second_name"),
                        rs.getBoolean("admin")
                ));
            }
        } catch (SQLException e) {
            logger.logError(e, "Data Base not contains field: ");
            throw new IllegalArgumentException("Data Base not contains field: ", e);
        }
        return users;
    }

    @Override
    public boolean delete(String id) {
        try {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM STUDENT where id = ?");
            ps.setInt(1, Integer.parseInt(id));
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.logError(e, "Delete is failed");
            throw new IllegalArgumentException("Delete is failed: ", e);
        }
    }
}
