package org.geekhub.lesson15.task1.servlets;

import org.geekhub.lesson15.task1.User;
import org.geekhub.lesson15.task1.model.DataBaseSource;
import org.geekhub.lesson15.task1.model.Source;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import static java.util.Objects.nonNull;

@WebServlet(name = "registration", urlPatterns = "/registration")
public class Registration extends HttpServlet {
    final static Source source = new DataBaseSource();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (nonNull(req.getQueryString())) {
            String id = req.getParameter("delete");
            if(nonNull(id)){
                source.delete(id);
            } else {
                String login = req.getParameter("login");
                String password = req.getParameter("password");
                String firstName = req.getParameter("firstName");
                String lastName = req.getParameter("lastName");
                Boolean admin = Boolean.getBoolean(req.getParameter("admin"));
                source.add(login, password, firstName, lastName, admin);
            }
        }
        List<User> users = source.getAllUser();
        session.setAttribute("users", users);

        req.getRequestDispatcher("WEB-INF/adminPanel.jsp").forward(req, resp);
    }
}
