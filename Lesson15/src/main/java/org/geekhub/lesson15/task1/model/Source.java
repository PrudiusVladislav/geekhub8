package org.geekhub.lesson15.task1.model;

import org.geekhub.lesson15.task1.User;

import java.util.List;

public interface Source {
    boolean contains(String login, String password);

    boolean add(String login, String password, String firstName, String lastName, Boolean isAdmin);

    List<User> getAllUser();

    boolean delete(String login);

    boolean isAdmin(String login, String password);

}
