package org.geekhub.lesson15.task1.servlets;

import org.geekhub.lesson15.task1.model.DataBaseSource;
import org.geekhub.lesson15.task1.model.Source;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Objects.nonNull;

@WebServlet(name = "authentication", urlPatterns = "/authentication")
public class Authentication extends HttpServlet {
    Source source = new DataBaseSource();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (nonNull(req.getQueryString())) {
            String login = req.getParameter("login");
            String password = req.getParameter("password");
            if (source.contains(login, password)) {
                if (source.isAdmin(login, password)) {
                    req.getRequestDispatcher("WEB-INF/adminPanel.jsp").forward(req, resp);
                } else {
                    req.getRequestDispatcher("WEB-INF/userPanel.jsp").forward(req, resp);
                }
            }
        }
        req.getRequestDispatcher("WEB-INF/authentication.jsp").forward(req, resp);
    }
}