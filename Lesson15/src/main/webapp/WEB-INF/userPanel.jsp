<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User panel</title>
</head>
<body>
<form  action='userpanel' method='get' >
    <td> <input type='submit' value='Show all users'></td>
    <fieldset>
        <legend>All users:</legend>
        <table border="1">
            <td>Id</td>
            <td>Login</td>
            <td>Password</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Is admin</td>

            <tr>
                <c:forEach var="users" items="${users}">
                <td>${users.id}</td>
                <td>${users.login}</td>
                <td>${users.password}</td>
                <td>${users.firstName}</td>
                <td>${users.lastName}</td>
                <td>${users.admin}</td>

            <tr>
                </c:forEach>
        </table>
    </fieldset>
</form>
</body>
</html>
