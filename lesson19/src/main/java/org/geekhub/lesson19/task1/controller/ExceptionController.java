package org.geekhub.lesson19.task1.controller;

import org.geekhub.lesson19.task1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


public class ExceptionController {
    private final UserService userService;

    @Autowired
    public ExceptionController(UserService userService) {
        this.userService = userService;
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public String getException(Exception exception, Model model) {
        model.addAttribute("errorMessage", exception.getMessage());
        return "error";
    }
}
