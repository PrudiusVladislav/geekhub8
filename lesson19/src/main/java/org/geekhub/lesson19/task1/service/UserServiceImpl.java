package org.geekhub.lesson19.task1.service;

import org.geekhub.lesson19.task1.db.persistence.User;
import org.geekhub.lesson19.task1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findBy(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findBy(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public Optional<User> findBy(String login, String password) {
        return userRepository.findByLoginAndPassword(login, password);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }
}
