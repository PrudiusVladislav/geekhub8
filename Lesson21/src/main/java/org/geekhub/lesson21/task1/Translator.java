package org.geekhub.lesson21.task1;

import org.geekhub.lesson10.logger.ConsoleLogger;
import org.geekhub.lesson10.logger.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Translator {
    private Logger logger = new ConsoleLogger(Translator.class);

    public String translate(String text) {
        Properties properties = getProperty();
        String length = "";
        String word = text.toUpperCase();

        for (int i = 0; i < word.length(); i++) {
            if (properties.containsKey(String.valueOf(word.charAt(i)))) {
                length += properties.getProperty(String.valueOf(word.charAt(i))) + " ";
            } else if (word.charAt(i) == ' ') {
                length += "/ ";
            } else {
                logger.logInfo("Properties not contains key");
                throw new IllegalArgumentException("Properties not contains key");
            }
        }

        return length;
    }

    private Properties getProperty() {
        Properties properties = new Properties();

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream("MorseAlphabet.properties")) {
            properties.load(stream);
        } catch (IOException e) {
            logger.logError(e, "Not read properties file");
        }

        return properties;
    }
}
