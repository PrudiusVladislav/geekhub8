package org.geekhub.lesson21.task1;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TranslatorTest {

    @Test
    public void shouldCorrectTranslateWord_SOS() {
        Translator translator = new Translator();

        String actual = translator.translate("SOS");
        String expected = "... --- ... ";

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldCorrectTranslateWord_S_O_S() {
        Translator translator = new Translator();

        String actual = translator.translate("S O S");
        String expected = "... / --- / ... ";

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldCorrectTranslateWord_GeekHub() {
        Translator translator = new Translator();

        String actual = translator.translate("GeekHub");
        String expected = "--. . . -.- .... ..- -... ";

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldCorrectTranslateWord_it_is_a_good_day_to_die() {
        Translator translator = new Translator();

        String actual = translator.translate("it is a good day to die");
        String expected = ".. - / .. ... / .- / --. --- --- -.. / -.. .- -.-- / - --- / -.. .. . ";

        Assert.assertEquals(actual, expected);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldReturnExceptionWhenWhenIncorrectInput() {
        Translator translator = new Translator();

        String actual = translator.translate("$0m*th!#g Br0k*#!");

    }
}