package org.geekhub.lesson4.task2.user;

import java.util.Optional;

public interface UserSource {
    Optional<User> find(String username);
}