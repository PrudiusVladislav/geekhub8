package org.geekhub.lesson4.task2.authentication;

import org.geekhub.lesson4.task2.authentication.exception.AuthException;
import org.geekhub.lesson4.task2.authentication.exception.UserNotFoundException;
import org.geekhub.lesson4.task2.authentication.exception.WrongCredentialsException;
import org.geekhub.lesson4.task2.authentication.exception.WrongPasswordException;
import org.geekhub.lesson4.task2.user.User;
import org.geekhub.lesson4.task2.user.UserSource;


public class AuthenticationService {
    UserSource inMemoryUserSource;

    public AuthenticationService(UserSource inMemoryUserSource) {
        this.inMemoryUserSource = inMemoryUserSource;
    }

    public User auth(String username, String password) throws AuthException {
        if ((username == null || password == null || username.trim().isEmpty()) || password.trim().isEmpty()) {
            throw new WrongCredentialsException();
        }

        if (!inMemoryUserSource.find(username).isPresent()) {
            throw new UserNotFoundException();
        }

        User user = inMemoryUserSource.find(username).get();

        if (!user.getPassword().equals(password)) {
            throw new WrongPasswordException();
        }

        return user;
    }
}