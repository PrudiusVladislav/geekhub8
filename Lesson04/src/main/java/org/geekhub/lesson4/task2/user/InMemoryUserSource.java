package org.geekhub.lesson4.task2.user;

import java.util.HashMap;
import java.util.Optional;

public class InMemoryUserSource implements UserSource {
    private HashMap<String, User> userSource = new HashMap<>();

    public InMemoryUserSource(User... users) {
        for (int i = 0; i < users.length; i++) {
            userSource.put(users[i].getUserName(), users[i]);
        }
    }

    @Override
    public Optional<User> find(String username) {
        if (userSource.containsKey(username)) {
            return Optional.of(userSource.get(username));
        } else {
            return Optional.empty();
        }
    }
}