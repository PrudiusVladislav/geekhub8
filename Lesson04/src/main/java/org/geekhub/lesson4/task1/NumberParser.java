package org.geekhub.lesson4.task1;

public class NumberParser {
    public static int parse(String str) throws NumberFormatException {
        int number = 0;

        if (str == null || str.trim().isEmpty()) {
            throw new NumberFormatException();
        }

        if (!isNumber(str)) {
            throw new NumberFormatException();
        }

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '+' || str.charAt(i) == '-') {
                continue;
            }
            number = number * 10 + str.charAt(i) - '0';
        }

        if (str.charAt(0) == '-') {
            number = 0 - number;
        }

        return number;
    }

    private static boolean isNumber(String str) {
        boolean isNumber = false;
        int index = 0;
        if (str.charAt(0) == '+' || str.charAt(0) == '-') {
            index = 1;
        }
        for (; index < str.length(); index++) {

            if (str.charAt(index) >= '0' && str.charAt(index) <= '9') {
                isNumber = true;
            } else {
                return false;
            }
        }
        return isNumber;
    }
}
