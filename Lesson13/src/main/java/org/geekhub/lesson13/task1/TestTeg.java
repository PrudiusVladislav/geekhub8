package org.geekhub.lesson13.task1;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.DynamicAttributes;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.List;
import java.util.Map;

public class TestTeg extends TagSupport implements DynamicAttributes {
    private static final long serialVersionUID = 1L;
    Storage storage = new Storage();

    List<Animal> animals = storage.getAnimals();
    @Override
    public int doStartTag() throws JspException {
        Map<String, List<Animal>> map = storage.getMap();

        return SKIP_BODY;
    }

    @Override
    public void setDynamicAttribute(String uri, String localName, Object value) throws JspException {
        animals.add(new Animal(localName,(String) value));
    }
}
