package org.geekhub.lesson13.task1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Storage {
   private List<Animal> animals = new ArrayList<>();
    private Map<String, List<Animal>> map = new HashMap<>();

    public List<Animal> getAnimals() {
        return animals;
    }

    public Map<String, List<Animal>> getMap() {
        return map;
    }
}
