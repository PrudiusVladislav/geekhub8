package org.geekhub.lesson13.task1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@WebServlet(name = "animalServlet", urlPatterns = "/userpage")
public class AnimalServlet extends HttpServlet {
    Storage storage = new Storage();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Animal> animals = storage.getAnimals();

        Map<String, List<Animal>> map = storage.getMap();

        String type = req.getParameter("type");
        String name = req.getParameter("name");

        if(!map.containsKey(type)) {
            animals = new ArrayList<>();
            animals.add(new Animal(type, name));
            map.put(type, animals);
        } else {
            animals.add(new Animal(type, name));
            map.put(type, animals);
        }

        req.setAttribute("animals", map);

        req.getRequestDispatcher("WEB-INF/table.jsp").forward(req, resp);
    }
}