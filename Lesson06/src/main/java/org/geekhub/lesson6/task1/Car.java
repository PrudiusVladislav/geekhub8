package org.geekhub.lesson6.task1;

import java.util.Objects;

public class Car implements Comparable<Car> {
    private String brand;
    private int power;
    private Color color;

    Car(String brand, int power, Color color) {
        this.brand = brand;
        this.power = power;
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return power == car.power &&
                Objects.equals(brand, car.brand) &&
                Objects.equals(color, car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, power, color);
    }

    @Override
    public int compareTo(Car o) {
        if (!this.brand.equals(o.brand)) {
            return this.brand.compareTo(o.brand);
        }

        if (this.power != o.power) {
            return this.power > o.power ? -1 : 1;
        }

        return this.color.compareTo(o.color);
    }
}