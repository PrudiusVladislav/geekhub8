package org.geekhub.lesson6.task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GarageImpl<T extends Car> implements Garage {
    private List<T> cars = new ArrayList();

    GarageImpl(T... cars) {
        this.cars.addAll(Arrays.asList(cars));
    }

    @Override
    public List<T> getCars() {
        List<T> cars = new ArrayList(this.cars);
        Collections.sort(cars);
        return cars;
    }
}