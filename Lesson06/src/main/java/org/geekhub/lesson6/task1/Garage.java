package org.geekhub.lesson6.task1;

import java.util.List;

public interface Garage<T> {
    List<T> getCars();
}
