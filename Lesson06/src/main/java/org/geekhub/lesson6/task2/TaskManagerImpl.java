package org.geekhub.lesson6.task2;

import org.geekhub.lesson6.task2.exception.TaskLimitExceededException;

import java.time.LocalDate;
import java.util.*;

public class TaskManagerImpl implements TaskManager, Iterable {

    private int tasksDailyLimit;
    private List<Task> tasks = new ArrayList<>();

    TaskManagerImpl() {
        this.tasksDailyLimit = Integer.MAX_VALUE;
    }

    TaskManagerImpl(int tasksDailyLimit) {
        if (tasksDailyLimit < 0) {
            throw new IllegalArgumentException();
        }
        this.tasksDailyLimit = tasksDailyLimit;
    }

    @Override
    public int addTask(Task task) {
        if (getTasksForDay(task.getDate()).size() >= tasksDailyLimit) {
            throw new TaskLimitExceededException();
        }
        tasks.add(task);
        return tasks.size();
    }

    private List<Task> getTasksForDay(LocalDate day) {
        List<Task> tasksForDay = new ArrayList<>();

        for (Task task : tasks) {
            if (task.getDate().equals(day)) {
                tasksForDay.add(task);
            }
        }
        return tasksForDay;
    }

    @Override
    public boolean removeTasks(LocalDate date) {
        boolean checkRemove = false;
        Iterator<Task> it = iterator();
        while (it.hasNext()) {
            Task task = it.next();
            if (task.getDate().equals(date)) {
                it.remove();
                checkRemove = true;
            }
        }
        return checkRemove;
    }

    @Override
    public Set<String> getCategories() {
        Set<String> categories = new HashSet<>();

        for (Task task : tasks) {
            categories.add(task.getCategory());
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> map = new HashMap<>();
        Set<String> categories = getCategories();

        for (String category : categories) {
            map.put(category, getTasksByCategory(category));
        }

        return map;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> workTasks = new ArrayList<>();

        for (Task task : tasks) {
            if (task.getCategory().equals(category)) {
                workTasks.add(task);
            }
        }
        Collections.sort(workTasks);
        return workTasks;
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> tasksForToday = new ArrayList<>();
        LocalDate toDay = LocalDate.now();
        for (Task task : tasks) {
            if (task.getDate().equals(toDay)) {
                tasksForToday.add(task);
            }
        }
        return tasksForToday;
    }

    @Override
    public List<Task> getAllTasks() {
        List<Task> allTasks = new ArrayList(tasks);
        return allTasks;
    }

    @Override
    public Iterator<Task> iterator() {
        return new Iterator<Task>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < tasks.size();
            }

            @Override
            public Task next() {
                return tasks.get(index++);
            }

            @Override
            public void remove() {
                tasks.remove(--index);
            }
        };
    }
}