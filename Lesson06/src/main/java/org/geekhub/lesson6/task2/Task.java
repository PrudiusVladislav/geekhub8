package org.geekhub.lesson6.task2;

import java.time.LocalDate;
import java.util.Objects;

public class Task implements Comparable<Task> {
    private final String category;
    private final String description;
    private final LocalDate date;

    public Task(String category, String description, LocalDate date) {
        this.category = category;
        this.description = description;
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Task{" +
                "category='" + category + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(category, task.category) &&
                Objects.equals(description, task.description) &&
                Objects.equals(date, task.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(category, description, date);
    }

    @Override
    public int compareTo(Task o) {
        return o.date.compareTo(this.date);
    }
}