package org.geekhub.lesson6.task2;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface TaskManager {
    int addTask(Task task);

    boolean removeTasks(LocalDate yesterday);

    Set<String> getCategories();

    Map<String, List<Task>> getTasksByCategories();

    List<Task> getTasksByCategory(String health);

    List<Task> getTasksForToday();

    List<Task> getAllTasks();
}