package org.geekhub.lesson2.task1;

public class StringUtils {
    public static void main(String[] args) {
        System.out.println(toggleCase(args[0]));
    }

    static String toggleCase(String str) {
        if (str == null) {
            return null;
        }

        if (str.isEmpty()) {
            return str;
        }

        char firstSymbol = str.charAt(0);
        if (firstSymbol >= 'a' && firstSymbol <= 'z') {
            return str.toUpperCase();
        } else if (firstSymbol >= 'A' && firstSymbol <= 'Z') {
            return str.toLowerCase();
        }

        return str;
    }
}
