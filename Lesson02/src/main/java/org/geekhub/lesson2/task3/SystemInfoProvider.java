package org.geekhub.lesson2.task3;

import java.io.File;
import java.lang.management.ManagementFactory;

public class SystemInfoProvider {
     int getProcessorsCount() {
        return Runtime.getRuntime().availableProcessors();
    }

     long getFreeRam() {
        long freeRam = ((com.sun.management.OperatingSystemMXBean) ManagementFactory
                .getOperatingSystemMXBean()).getFreePhysicalMemorySize();
        return freeRam;
    }

     long getTotalRam() {
        long totalRam = ((com.sun.management.OperatingSystemMXBean) ManagementFactory
                .getOperatingSystemMXBean()).getTotalPhysicalMemorySize();
        return totalRam;
    }

     int getJavaVersion() {
         String version =System.getProperty("java.version");
         if (version.startsWith("1.")) {
             version = version.substring(2);
             if(version.contains(".")) {
                 version = version.substring(0, version.indexOf('.'));
             }
         } else if (version.contains(".")) {
             version = version.substring(0, version.indexOf('.'));
         }
         return Integer.parseInt(version);
    }

     String getUserName() {
        return System.getProperty("user.name");
    }

     String getUserHomeDirectory() {
        return System.getProperty("user.home");
    }

     String getOSName() {
        return System.getProperty("os.name");
    }

    Drive[] getStorageInfo() {
         File[] folders = File.listRoots();
         Drive[] drives = new Drive[folders.length];

        for (int i = 0; i < folders.length; i++) {
            drives[i] = new Drive(folders[i].getPath(),
                    bytesToMegabytes(folders[i].getFreeSpace()),
                    bytesToMegabytes(folders[i].getTotalSpace() - folders[i].getFreeSpace()),
                    bytesToMegabytes(folders[i].getTotalSpace()));
        }
         return drives;
    }

    float bytesToMegabytes(long bytes) {
        return bytes / (1024*1024);
    }
}
