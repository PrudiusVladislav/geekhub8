package org.geekhub.lesson2.task3;

public class Drive {
    String name;
    float freeMemory;
    float usedMemory;
    float totalMemory;

    public Drive(String name, float freeMemory, float usedMemory, float totalMemory) {
        this.name = name;
        this.freeMemory = freeMemory;
        this.usedMemory = usedMemory;
        this.totalMemory = totalMemory;
    }

    @Override
    public String toString() {
        return "Drive{" +
                "name='" + name + '\'' +
                ", freeMemory=" + freeMemory + " MB"+
                ", usedMemory=" + usedMemory + " MB"+
                ", totalMemory=" + totalMemory + " MB"+
                '}';
    }
}
