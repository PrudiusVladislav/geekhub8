package org.geekhub.lesson2.task3;

public class SystemProvider {
    public static void main(String[] args) {
        SystemInfoProvider systemInfoProvider = new SystemInfoProvider();

        System.out.println("Processors = " + systemInfoProvider.getProcessorsCount());
        System.out.println("Free RAM = " + systemInfoProvider.
                bytesToMegabytes(systemInfoProvider.getFreeRam() ) + "MB");
        System.out.println("Total RAM = " + systemInfoProvider.
                bytesToMegabytes(systemInfoProvider.getTotalRam()) + "MB");
        System.out.println("Java version = " + systemInfoProvider.getJavaVersion());
        System.out.println("User name = " + systemInfoProvider.getUserName());
        System.out.println("User home directory = " + systemInfoProvider.getUserHomeDirectory());
        System.out.println("OS name = " + systemInfoProvider.getOSName());

        for (Drive drive :systemInfoProvider.getStorageInfo()) {
            System.out.println(drive.toString());
        }
    }
}
