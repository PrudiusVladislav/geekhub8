package org.geekhub.lesson2.task2;

public class StringRepeaterStringConcatImpl implements StringRepeater {
    @Override
    public String repeat(String str, int times) {
        String strToRepeat = "";
        for (int i = 0; i < times; i++) {
            strToRepeat = strToRepeat.concat(str);
        }
        return strToRepeat;
    }
}
