package org.geekhub.lesson2.task2;

public interface StringRepeater {
    String repeat(String str, int times);
}
