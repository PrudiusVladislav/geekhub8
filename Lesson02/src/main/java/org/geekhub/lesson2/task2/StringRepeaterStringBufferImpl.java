package org.geekhub.lesson2.task2;

public class StringRepeaterStringBufferImpl implements StringRepeater {
    @Override
    public String repeat(String str, int times) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < times; i++) {
            stringBuffer.append(str);
        }
        return stringBuffer.toString();
    }
}
