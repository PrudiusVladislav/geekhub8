package org.geekhub.lesson2.task2;

public class MainStringRepeater {
    public static void main(String[] args) {
        MainStringRepeater mainStringRepeater =  new MainStringRepeater();
        mainStringRepeater.checkSpeed(new StringRepeaterStringConcatImpl(), args);
        mainStringRepeater.checkSpeed(new StringRepeaterStringBufferImpl(), args);
        mainStringRepeater.checkSpeed(new StringRepeaterStringAppendImpl(), args);
        mainStringRepeater.checkSpeed(new StringRepeaterStringBuilderImpl(), args);

    }

    private void checkSpeed(StringRepeater stringRepeater, String[] args) {
        long countTime = System.nanoTime();
        stringRepeater.repeat(args[0],Integer.parseInt(args[1]));
        System.out.println(System.nanoTime() - countTime);
    }
}
