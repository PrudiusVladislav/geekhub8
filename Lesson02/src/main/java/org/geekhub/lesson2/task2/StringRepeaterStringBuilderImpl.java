package org.geekhub.lesson2.task2;

public class StringRepeaterStringBuilderImpl implements StringRepeater {
    @Override
    public String repeat(String str, int times) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < times; i++) {
            stringBuilder.append(str);
        }
        return stringBuilder.toString();
    }
}
