package org.geekhub.lesson2.task2;

public class StringRepeaterStringAppendImpl implements StringRepeater {
    @Override
    public String repeat(String str, int times) {
        String strToRepeat = "";
        for (int i = 0; i < times; i++) {
            strToRepeat += str;
        }
        return strToRepeat;
    }
}
