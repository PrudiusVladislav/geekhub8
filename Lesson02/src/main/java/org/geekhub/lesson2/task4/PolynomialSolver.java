package org.geekhub.lesson2.task4;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class PolynomialSolver {
    static Optional<Set<Integer>> findRoots(int a, int b, int c) {
        Set<Integer> roots = new HashSet<>();
        int x1 = 0;
        int x2 = 0;
        double discriminant = findDiscriminant(a, b, c);

        if (discriminant > 0) {
            x1 = (int) (-b + Math.sqrt(discriminant)) / (2 * a);
            x2 = (int) (-b - Math.sqrt(discriminant)) / (2 * a);
            roots.add(x1);
            roots.add(x2);
        } else if (discriminant == 0) {
            x1 = -b / (2 * a);
            roots.add(x1);
        } else {
            return Optional.empty();
        }

        return Optional.of(roots);
    }

    private static double findDiscriminant(int a, int b, int c) {
        return Math.pow(b, 2) - (4 * a * c);
    }
}
