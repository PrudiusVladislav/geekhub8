package org.geekhub.lesson2.task4;

public class Main {
    public static void main(String[] args) {
        PolynomialSolver polynomialSolver = new PolynomialSolver();
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int c = Integer.parseInt(args[2]);
        System.out.println(polynomialSolver.findRoots(a, b, c));
    }
}
