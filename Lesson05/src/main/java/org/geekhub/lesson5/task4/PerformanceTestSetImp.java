package org.geekhub.lesson5.task4;

import java.util.Set;

public class PerformanceTestSetImp implements PerformanceTest {
    private Set<String> set;
    private String str;

    PerformanceTestSetImp(Set<String> set, String str) {
        this.set = set;
        this.str = str;
    }

    @Override
    public void performanceTest() {
        addElement();
        containsElement();
        removeElement();
    }

    private void addElement() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            set.add(str);
        }
        System.out.print("add - ");
        System.out.println(System.nanoTime() - countTime);
    }

    private void containsElement() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            set.contains(str);
        }
        System.out.print("contains - ");
        System.out.println(System.nanoTime() - countTime);
    }

    private void removeElement() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            set.remove(str);
        }
        System.out.print("remove - ");
        System.out.println(System.nanoTime() - countTime);
    }
}