package org.geekhub.lesson5.task4;

import java.util.Map;

public class PerformanceTestMapImp implements PerformanceTest {
    private Map<String, String> map;
    private String str;

     PerformanceTestMapImp(Map<String, String> map, String str) {
        this.map = map;
        this.str = str;
    }

    @Override
    public void performanceTest() {
        insertion();
        update();
        contains();
        removal();
    }

    private void insertion() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            map.put(str, str);
        }
        System.out.print("add - ");
        System.out.println(System.nanoTime() - countTime);

    }

    private void update() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            map.replace(str, str);
        }
        System.out.print("update - ");
        System.out.println(System.nanoTime() - countTime);
    }

    private void contains() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            map.containsKey(str);
        }
        System.out.print("contains - ");
        System.out.println(System.nanoTime() - countTime);
    }

    private void removal() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            map.remove(str);
        }
        System.out.print("remove - ");
        System.out.println(System.nanoTime() - countTime);
    }
}