package org.geekhub.lesson5.task4;

import java.util.*;

public class MainPerformanceTest {
    public static void main(String[] args) {

        performanceTest(new PerformanceTestListImp(new ArrayList<>(), "str"));
        performanceTest(new PerformanceTestListImp(new LinkedList<>(), "str"));
        performanceTest(new PerformanceTestListImp(new Vector<>(), "str"));

        performanceTest(new PerformanceTestMapImp(new HashMap<>(), "str"));
        performanceTest(new PerformanceTestMapImp(new LinkedHashMap<>(), "str"));
        performanceTest(new PerformanceTestMapImp(new TreeMap<>(), "str"));

        performanceTest(new PerformanceTestSetImp(new HashSet<>(), "str"));
        performanceTest(new PerformanceTestSetImp(new LinkedHashSet<>(), "str"));
        performanceTest(new PerformanceTestSetImp(new TreeSet<>(), "str"));
    }

    private static void performanceTest(PerformanceTest performanceTest) {
        performanceTest.performanceTest();
    }
}