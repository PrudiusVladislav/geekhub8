package org.geekhub.lesson5.task4;

import java.util.*;

public class PerformanceTestListImp implements PerformanceTest {
    private List<String> list;
    private String str;

    PerformanceTestListImp(List<String> list, String str) {
        this.list = list;
        this.str = str;
    }

    @Override
    public void performanceTest() {
        addElement();
        getElement();
        setElement();
        removeElement();
    }

    private void addElement() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.add(0, str);
        }
        System.out.print("add beginning - ");
        System.out.println(System.nanoTime() - countTime);

        countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.add(list.size() / 2, str);
        }
        System.out.print("add middle - ");
        System.out.println(System.nanoTime() - countTime);

        countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.add(list.size(), str);
        }
        System.out.print("add end - ");
        System.out.println(System.nanoTime() - countTime);
    }

    private void getElement() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.get(0);
        }
        System.out.print("get beginning - ");
        System.out.println(System.nanoTime() - countTime);

        countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.get(list.size() / 2);
        }
        System.out.print("get middle - ");
        System.out.println(System.nanoTime() - countTime);

        countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.get(list.size() - 1);
        }
        System.out.print("get end - ");
        System.out.println(System.nanoTime() - countTime);
    }

    private void setElement() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.set(0, str);
        }
        System.out.print("set beginning - ");
        System.out.println(System.nanoTime() - countTime);

        countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.set(list.size() / 2, str);
        }
        System.out.print("set middle - ");
        System.out.println(System.nanoTime() - countTime);

        countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.set(list.size() - 1, str);
        }
        System.out.print("set end - ");
        System.out.println(System.nanoTime() - countTime);
    }

    private void removeElement() {
        long countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.remove(0);
        }
        System.out.print("remove beginning - ");
        System.out.println(System.nanoTime() - countTime);

        countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.remove(list.size() / 2);
        }
        System.out.print("remove middle - ");
        System.out.println(System.nanoTime() - countTime);

        countTime = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.remove(list.size() - 1);
        }
        System.out.print("remove end - ");
        System.out.println(System.nanoTime() - countTime);
    }
}