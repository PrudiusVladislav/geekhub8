package org.geekhub.lesson5.task3;

import java.util.Objects;

public class Vote {
    private String name;
    private int candidate;

    public Vote(int candidate) {
        this.candidate = candidate;
    }

    public int getCandidate() {
        return candidate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return candidate == vote.candidate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(candidate);
    }

    @Override
    public String toString() {
        return "Vote{" +
                "candidate=" + candidate +
                '}';
    }
}
