package org.geekhub.lesson5.task3;

import java.util.Collection;
import java.util.Iterator;

public class VotingBox implements Collection<Vote> {
    private int extraPaidCandidate;
    private int nonPaidCandidate;
    private Vote[] votesBox;

    public VotingBox(int extraPaidCandidate, int nonPaidCandidate) {
        this.extraPaidCandidate = extraPaidCandidate;
        this.nonPaidCandidate = nonPaidCandidate;
        this.votesBox = new Vote[0];
    }

    @Override
    public int size() {
        return votesBox.length;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(Object o) {
        for (Vote vote : votesBox) {
            if (vote.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<Vote> iterator() {
        return new Iterator<Vote>() {
            private int index = 0;
            Vote[] vote = votesBox;

            @Override
            public boolean hasNext() {
                return index < vote.length;
            }

            @Override
            public Vote next() {
                return vote[index++];
            }
        };
    }

    @Override
    public Object[] toArray() {
        return new Object[size()];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Vote vote) {
        if (vote.getCandidate() == extraPaidCandidate) {
            addCandidate(vote);
            addCandidate(vote);
        } else if (vote.getCandidate() != nonPaidCandidate) {
            addCandidate(vote);
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < votesBox.length; i++) {
            if (votesBox[i].equals(o)) {
                Vote[] votes = votesBox;
                votesBox = new Vote[votes.length - 1];
                System.arraycopy(votes, 0, votesBox, 0, i);
                int amountElemAfterIndex = votes.length - i - 1;
                System.arraycopy(votes, i + 1, votesBox, i, amountElemAfterIndex);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object elem : c) {
            if (!contains(elem)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Vote> c) {
        for (Vote vote : c) {
            add(vote);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object vote : c) {
            while (contains(vote)) {
                remove(vote);
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Object elem : c) {
            for (Vote vote : votesBox) {
                if (!vote.equals(elem)) {
                    remove(vote);
                }
            }
        }
        return true;
    }

    @Override
    public void clear() {
        votesBox = new Vote[0];
    }

    private boolean addCandidate(Vote vote) {
        Vote[] votes = votesBox;
        votesBox = new Vote[votes.length + 1];
        System.arraycopy(votes, 0, votesBox, 0, votes.length);
        votesBox[votesBox.length - 1] = vote;
        return true;
    }
}