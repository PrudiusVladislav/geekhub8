package org.geekhub.lesson5.task1;

import java.util.Objects;

public class Color {
    final static Color RED = new Color(255, 0, 0);
    final static Color GREEN = new Color(0, 255, 0) ;
    final static Color BLUE = new Color(0, 0, 255);

    private int red;
    private int green;
    private int blue;

    public Color(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Color color = (Color) o;
        return red == color.red &&
                green == color.green &&
                blue == color.blue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(red, green, blue);
    }
}