package org.geekhub.lesson5.task1;

import java.util.Objects;

public class Car {
    private String name;
    private int power;
    private String model;
    private Color color;

    protected Car(String name, int power, String model, Color color) {
        this.name = name;
        this.power = power;
        this.model = model;
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return power == car.power &&
                Objects.equals(model, car.model) &&
                Objects.equals(color, car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(power, model, color);
    }
}