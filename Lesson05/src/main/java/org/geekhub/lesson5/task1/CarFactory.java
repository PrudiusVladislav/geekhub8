package org.geekhub.lesson5.task1;

public class CarFactory {
    public static Car create(String type, int power, String name, Color color) {
        return new Car(type, power, name, color);
    }
}