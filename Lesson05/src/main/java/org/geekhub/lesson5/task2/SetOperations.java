package org.geekhub.lesson5.task2;

import java.util.Set;

public interface SetOperations<Object> {
    boolean equals(Set<Object> set1, Set<Object> set2);

    Set<Object> union(Set<Object> set1, Set<Object> set2);

    Set<Object> subtract(Set<Object> set1, Set<Object> set2);

    Set<Object> intersect(Set<Object> set1, Set<Object> set2);

    Set<Object> symmetricSubtract(Set<Object> set1, Set<Object> set2);
}
