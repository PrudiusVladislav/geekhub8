package org.geekhub.lesson5.task2;

import java.util.HashSet;
import java.util.Set;

public class SetOperationsImpl<Object> implements SetOperations<Object> {

    @Override
    public boolean equals(Set<Object> set1, Set<Object> set2) {

        if (set1 == null && set2 == null) {
            return true;
        } else if (set1 == null || set2 == null) {
            return false;
        }

        if (set1.size() != set2.size()) {
            return false;
        }
        for (Object elemOfSet1 : set1) {
            if (!set2.contains(elemOfSet1)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Set<Object> union(Set<Object> set1, Set<Object> set2) {
        Set set = new HashSet(set1);
        set.addAll(set2);
        return set;
    }

    @Override
    public Set<Object> subtract(Set<Object> set1, Set<Object> set2) {
        Set set = new HashSet(set1);
        set.removeAll(set2);
        return set;
    }

    @Override
    public Set<Object> intersect(Set<Object> set1, Set<Object> set2) {
        Set set = new HashSet(set1);
        set.retainAll(set2);
        return set;
    }

    @Override
    public Set<Object> symmetricSubtract(Set<Object> set1, Set<Object> set2) {
        Set copySet1 = new HashSet(set1);
        Set copySet2 = new HashSet(set2);


        if (set1.isEmpty() && set2.isEmpty()) {
            return union(set1, set2);
        }
        if (set1.containsAll(set2)) {
            return subtract(set1, set2);
        } else if (set2.containsAll(set1)) {
            return subtract(set2, set1);
        } else {
            return union(set1, set2);
        }
    }
}