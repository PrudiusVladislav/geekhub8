package org.geekhub.lesson20.task1.controller;

import org.geekhub.lesson20.task1.db.persistence.User;
import org.geekhub.lesson20.task1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegistrationController {
    private final UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/registration")
    public String save(Model model, User user) {
        User savedUser = userService.save(user);

        model.addAttribute("users", userService.getAllUser());
        return "main";
    }

    @PostMapping("/delete")
    public String deleteById(Integer id, Model model) {
        userService.delete(id);

        model.addAttribute("users", userService.getAllUser());

        return "main";
    }

    @PostMapping("/update")
    public String update(User user, Model model) {
        userService.save(user);

        model.addAttribute("users", userService.getAllUser());

        return "main";
    }
}
