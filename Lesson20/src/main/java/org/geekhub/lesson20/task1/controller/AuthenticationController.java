package org.geekhub.lesson20.task1.controller;

import org.geekhub.lesson20.task1.db.persistence.User;
import org.geekhub.lesson20.task1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
public class AuthenticationController {
    private final UserService userService;

    @Autowired
    public AuthenticationController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/")
    public String index() {
        return "authentication";
    }


    @PostMapping("/authentication")
    public String authentication(String login, String password, Model model) {
        Optional<User> user = userService.findBy(login, password);

        if(user.isPresent()) {
            User presentUser = user.get();
            if(presentUser.getAdmin()) {
                model.addAttribute("users", userService.getAllUser());
                return "main";
            }
        }
        throw new IllegalArgumentException("Incorrect data");
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public String getException(Exception exception, Model model) {
        model.addAttribute("errorMessage", exception.getMessage());
        return "error";
    }
}
