package org.geekhub.lesson20.task1.service;

import org.geekhub.lesson20.task1.db.persistence.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> findBy(Integer id);

    Optional<User> findBy(String login);

    Optional<User> findBy(String login, String password);

    User save(User user);

    void delete(User user);

    void delete(Integer id);

    List<User> getAllUser();

}
