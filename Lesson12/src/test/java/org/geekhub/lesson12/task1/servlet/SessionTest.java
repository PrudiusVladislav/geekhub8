package org.geekhub.lesson12.task1.servlet;

import org.geekhub.lesson12.task1.view.Session;
import org.mockito.Mockito;
import org.testng.annotations.Test;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import static org.testng.Assert.*;

public class SessionTest extends Mockito {
    @Test
    public void shouldReturnHtmlPage() throws IOException {
        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);


        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        Session servlet = new Session();

        when(request.getParameter("action")).thenReturn("ADD");
        when(request.getParameter("name")).thenReturn("Zordi");
        when(request.getParameter("value")).thenReturn("12");
        when(request.getSession()).thenReturn(session);
        when(response.getWriter()).thenReturn(printWriter);
        when(request.getQueryString()).thenReturn("?action=ADD&name=Zordi&value=12");
        servlet.doGet(request, response);

        assertEquals(stringWriter.toString(), getHtml());
    }

    private static String getHtml() {
        return  "<html>" +
                "<head></head>" +
                "<body>" +
                "Action:<br />" +
                "<form action='session' method='get'>" +
                "<select name='action'>" +
                "<option></option>" +
                "<option value='ADD'>Add</option>" +
                "<option value='UPDATE'>Update</option>" +
                "<option value='INVALIDATE'>Invalidate</option>" +
                "<option value='REMOVE'>Remove</option>" +
                "</select>" +
                "<br />" +
                "Name:<br />" +
                "<input type='text' name='name'>" +
                "<br />Value:<br />" +
                "<input type='text' name='value'>" +
                "<br />" +
                "<input type='Submit' >" +
                "<br />" +
                "Zordi" + "=" + "12" +
                "</form>" +
                "</body>" +
                "</html>";
    }
}