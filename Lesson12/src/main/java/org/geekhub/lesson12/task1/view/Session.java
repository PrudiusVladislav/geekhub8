package org.geekhub.lesson12.task1.view;

import org.geekhub.lesson12.task1.controller.Action;
import org.geekhub.lesson12.task1.controller.ActionType;
import org.geekhub.lesson12.task1.model.SessionSource;
import org.geekhub.lesson12.task1.model.Source;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static java.util.Objects.nonNull;
import static org.geekhub.lesson12.task1.controller.ActionFactory.createAction;

@WebServlet(name = "session", urlPatterns = "/session")
public class Session extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final HttpSession session = req.getSession();
        final Source source = new SessionSource(session);
        String name = "";
        String value = "";
        String actionFromClient = "";

        if (nonNull(req.getQueryString())) {
            actionFromClient = req.getParameter("action");
            name = req.getParameter("name");
            value = req.getParameter("value");
        }

        ActionType actionType = ActionType.recognize(actionFromClient);
        Action action = createAction(actionType);
        action.apply(source, name, value);
        String htmlPage = getHtmlPage(name, value);
        resp.getWriter().write(htmlPage);
    }

    private static String getHtmlPage(String name, String value) throws IOException {
        return "<html>" +
                "<head></head>" +
                "<body>" +
                "Action:<br />" +
                "<form action='session' method='get'>" +
                "<select name='action'>" +
                "<option></option>" +
                "<option value='ADD'>Add</option>" +
                "<option value='UPDATE'>Update</option>" +
                "<option value='INVALIDATE'>Invalidate</option>" +
                "<option value='REMOVE'>Remove</option>" +
                "</select>" +
                "<br />" +
                "Name:<br />" +
                "<input type='text' name='name'>" +
                "<br />Value:<br />" +
                "<input type='text' name='value'>" +
                "<br />" +
                "<input type='Submit' >" +
                "<br />" +
                name + "=" + value +
                "</form>" +
                "</body>" +
                "</html>"
                ;
    }
}