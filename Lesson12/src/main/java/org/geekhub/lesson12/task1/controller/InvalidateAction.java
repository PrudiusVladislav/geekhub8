package org.geekhub.lesson12.task1.controller;

import org.geekhub.lesson12.task1.model.Source;

public class InvalidateAction implements Action {
    @Override
    public void apply(Source source, String name, String value) {
        source.invalidate();
    }
}
