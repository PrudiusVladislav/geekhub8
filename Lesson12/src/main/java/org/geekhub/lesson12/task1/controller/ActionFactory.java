package org.geekhub.lesson12.task1.controller;

import java.util.Map;

import static java.util.Objects.nonNull;

public class ActionFactory {
    private static final Map<ActionType, Action> actions = Map.of(
            ActionType.ADD, new AddAction(),
            ActionType.REMOVE, new DeleteAction(),
            ActionType.UPDATE, new UpdateAction(),
            ActionType.INVALIDATE, new InvalidateAction()
    );

    public static Action createAction(ActionType actionType) {
        final Action action = actions.get(actionType);

        if(nonNull(action)){
            return action;
        }
        throw new IllegalArgumentException("Action not supported: " + actionType);
    }
}
