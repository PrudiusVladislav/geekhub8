package org.geekhub.lesson12.task1.model;

public interface Source {
    void put(String name, String value);

    void delete(String name);

    void invalidate();

}
