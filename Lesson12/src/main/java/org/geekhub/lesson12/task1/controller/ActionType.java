package org.geekhub.lesson12.task1.controller;

public enum ActionType {
    ADD, REMOVE, UPDATE, INVALIDATE;

    public static ActionType recognize(String actionString) {
        for(ActionType action : values()) {
            if(action.name().equals(actionString)) {
                return action;
            }
        }
        throw new IllegalArgumentException("Action not recognized: " + actionString);
    }
}

