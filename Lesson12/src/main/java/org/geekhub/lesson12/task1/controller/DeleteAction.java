package org.geekhub.lesson12.task1.controller;

import org.geekhub.lesson12.task1.model.Source;

public class DeleteAction implements Action {
    @Override
    public void apply(Source source, String name, String value) {
        source.delete(name);
    }
}
