package org.geekhub.lesson10.logger;

public interface Logger {
    void logError(Exception exception, String message);

    void logInfo(String message);

}
