package org.geekhub.lesson10.logger;

import java.util.Date;

public class ConsoleLogger implements Logger {
    private final Class classForLog;


    public ConsoleLogger(Class classForLog) {
        this.classForLog = classForLog;
    }

    @Override
    public void logError(Exception exception, String message) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(new Date(System.currentTimeMillis()));
        stringBuilder.append("::");
        stringBuilder.append(classForLog.getName());
        stringBuilder.append("::");
        stringBuilder.append(message);
        stringBuilder.append("::");
        stringBuilder.append(exception);
        System.out.println(stringBuilder);
    }

    @Override
    public void logInfo(String message) {
        System.out.println(message);
    }

}
