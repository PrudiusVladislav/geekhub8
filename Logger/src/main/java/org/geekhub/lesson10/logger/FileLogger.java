package org.geekhub.lesson10.logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;

public class FileLogger implements Logger {
    private final Class classForLog;

    Path logFile = Path.of("Logger/src/main/java/org/geekhub/lesson10/resources/fileForLog.txt");

    public FileLogger(Class classForLog) {
        this.classForLog = classForLog;
    }

    @Override
    public void logError(Exception exception, String message) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(new Date(System.currentTimeMillis()));
        stringBuilder.append("::");
        stringBuilder.append(classForLog.getName());
        stringBuilder.append("::");
        stringBuilder.append(message);
        stringBuilder.append("::");
        stringBuilder.append(exception);

        writeLog(stringBuilder.toString().getBytes());

    }

    @Override
    public void logInfo(String message) {
        writeLog(message.getBytes());
    }

    private void writeLog(byte[] message) {
        try {
            Files.write(logFile, message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
