package org.geekhub.lesson18.task1.repository;

import org.geekhub.lesson18.task1.dto.User;
import org.geekhub.lesson18.task1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {
  
    private UserService userService;

    @Autowired
    public UserRepository(UserService userService) {
        this.userService = userService;
    }


    public User findByLoginAndPassword(String login, String password) {
        return userService.findByLoginAndPassword(login, password);
    }

  
    public List<User> findAll() {
        return userService.findAll();
    }


    public int save(User user) {
        return userService.save(user);

    }


    public int delete(int id) {
        return userService.delete(id);
    }


    public int update(User user) {
        return userService.update(user);
    }
}