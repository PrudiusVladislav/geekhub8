package org.geekhub.lesson18.task1.service;

import org.geekhub.lesson18.task1.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserServiceImpl implements UserService {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public UserServiceImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public User findByLoginAndPassword(String login, String password) {
        return jdbcTemplate.queryForObject(
                "SELECT * FROM users WHERE login = :userLogin AND password = :userPassword",
                new MapSqlParameterSource("userLogin", login)
                        .addValue("userPassword", password),
                new BeanPropertyRowMapper<>(User.class)
        );
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(
                "SELECT * FROM users",
                (rs, rowNum) -> {
                    User user = new User();
                    user.setId(rs.getInt("id"));
                    user.setLogin(rs.getString("login"));
                    user.setPassword(String.valueOf(rs.getString("password").hashCode()));
                    user.setFirstName(rs.getString("first_name"));
                    user.setLastName(rs.getString("last_name"));
                    user.setAdmin(rs.getBoolean("admin"));
                    return user;
                }
        );
    }

    @Override
    public int save(User user) {
        return jdbcTemplate.update("INSERT INTO USERS(login, password, first_name, last_name, admin) VALUES (:login, :password, :first_name, :last_name, :admin)",
                new MapSqlParameterSource("admin", user.getAdmin())
                        .addValue("login", user.getLogin())
                        .addValue("password", user.getPassword())
                        .addValue("first_name", user.getFirstName())
                        .addValue("last_name", user.getLastName())

        );

    }

    @Override
    public int delete(int id) {
        return jdbcTemplate.update("DELETE FROM users WHERE id=:userId",
                new MapSqlParameterSource("userId", id));
    }

    @Override
    public int update(User user) {
        return jdbcTemplate.update("UPDATE users SET login=:login, password=:password, first_name=:first_name, last_name=:last_name, admin=:admin WHERE id= :userId",
                new MapSqlParameterSource("login", user.getLogin())
                        .addValue("password", user.getPassword())
                        .addValue("first_name", user.getFirstName())
                        .addValue("last_name", user.getLastName())
                        .addValue("admin", user.getAdmin())
                        .addValue("userId", user.getId())
        );
    }
}
