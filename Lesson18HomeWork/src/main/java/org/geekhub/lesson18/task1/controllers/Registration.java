package org.geekhub.lesson18.task1.controllers;

import org.geekhub.lesson18.task1.dto.User;
import org.geekhub.lesson18.task1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Registration {
    private UserRepository userRepository;

    @Autowired
    public Registration(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/registration")
    public String registration(User user, Model model) {
        userRepository.save(user);

        Iterable<User> users = userRepository.findAll();

        model.addAttribute("users", users);

        return "main";
    }

    @PostMapping("/update")
    public String update(User user, Model model) {
        userRepository.update(user);

        Iterable<User> users = userRepository.findAll();

        model.addAttribute("users", users);

        return "main";
    }

    @PostMapping("/delete")
    public String delete(User user, Model model) {
        userRepository.delete(user.getId());

        Iterable<User> users = userRepository.findAll();

        model.addAttribute("users", users);

        return "main";
    }
}