package org.geekhub.lesson18.task1.service;

import org.geekhub.lesson18.task1.dto.User;

import java.util.List;

public interface UserService {

    User findByLoginAndPassword(String login, String password);

    List<User> findAll();

    int save(User user);

    int delete(int id);

    int update(User user);
}
