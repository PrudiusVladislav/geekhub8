package org.geekhub.lesson18.task1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:db.properties")
public class DatabaseConfig {
    @Bean
    @Primary
    public DataSource dataSource1(Environment environment) {
        final SimpleDriverDataSource dataSource = new SimpleDriverDataSource();

        dataSource.setDriverClass(org.postgresql.Driver.class);
        dataSource.setUrl(environment.getProperty("database.url"));
        dataSource.setUsername(environment.getProperty("database.username"));
        dataSource.setPassword(environment.getProperty("database.password"));

        return dataSource;
    }

    @Bean
    public NamedParameterJdbcTemplate jdbcTemplate(DataSource dataSource) {
        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(dataSource);
        template.getJdbcTemplate().execute("CREATE TABLE IF NOT exists users(ID SERIAL PRIMARY KEY, LOGIN VARCHAR(15), PASSWORD VARCHAR(20), FIRST_NAME VARCHAR(20), LAST_NAME VARCHAR(20), ADMIN BOOLEAN)");
        template.getJdbcTemplate().execute("INSERT INTO users(id, admin, first_name, last_name, login, password) VALUES(1, true, 'admin', 'admin', 'admin', 'admin') ON CONFLICT (id) DO UPDATE SET admin=true, first_name='admin', last_name = 'admin',login='admin', password='admin';");
        return template;
    }
}
