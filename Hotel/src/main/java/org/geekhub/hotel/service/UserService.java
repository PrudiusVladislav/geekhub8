package org.geekhub.hotel.service;

import org.geekhub.hotel.domain.User;
import org.geekhub.hotel.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }


    public Optional<User> findBy(String username) {
       return userRepo.findByUsername(username);
    }


    public List<User> findAllUsers() {
        return (List<User>) userRepo.findAll();
    }
}
