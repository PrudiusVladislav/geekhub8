package org.geekhub.hotel.service;

import org.geekhub.hotel.domain.Product;
import org.geekhub.hotel.repository.ProductRepo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductService {
    private ProductRepo productRepo;

    public ProductService(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public List<Product> getAllProduct() {
        return(List<Product>) productRepo.findAll();
    }

    public Product saveProduct(Product product) {
        return productRepo.save(product);
    }
}
