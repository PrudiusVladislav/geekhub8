package org.geekhub.hotel.service;

import org.geekhub.hotel.domain.Role;
import org.geekhub.hotel.domain.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserService userService;

    public UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user1 = new User(1, username, "admin", Collections.singleton(Role.USER));

       //final Optional<User> optionalUser = Optional.of(user1);
        final Optional<User> optionalUser = userService.findBy(username);

        org.springframework.security.core.userdetails.User.UserBuilder userBuilder = null;
        if (optionalUser.isPresent()) {

            final User user = optionalUser.get();
            userBuilder = org.springframework.security.core.userdetails.User.withUsername(user.getUsername());
            userBuilder.password(new BCryptPasswordEncoder().encode(user.getPassword()));
            userBuilder.roles(Role.USER.name());
            return userBuilder.build();

        } else {
            throw new UsernameNotFoundException("Incorrect login or password.");
        }
    }



}
