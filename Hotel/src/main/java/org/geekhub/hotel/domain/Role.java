package org.geekhub.hotel.domain;

public enum Role {
    USER, MODERATOR, ADMINISTRATOR;
}
