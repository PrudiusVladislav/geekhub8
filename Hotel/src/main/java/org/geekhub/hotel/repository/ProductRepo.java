package org.geekhub.hotel.repository;

import org.geekhub.hotel.domain.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepo extends CrudRepository<Product, Long> {
}
