package org.geekhub.hotel.repository;

import org.geekhub.hotel.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepo extends CrudRepository<User, Long> {
    //User findByUsername(String username);
     Optional<User> findByUsername(String username);
}

