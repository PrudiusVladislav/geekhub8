package org.geekhub.hotel.controller;

import org.geekhub.hotel.domain.Role;
import org.geekhub.hotel.domain.User;
import org.geekhub.hotel.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;
import java.util.Optional;

@Controller
public class RegistrationController {

    private UserRepo userRepo;

    @Autowired
    public RegistrationController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user, Model model) {

        Optional<User> userFromDb = userRepo.findByUsername(user.getUsername());

        if(userFromDb.isPresent()){
            model.addAttribute("message", "User exists!");
            return "registration";
        }

        user.setRole(Collections.singleton(Role.ADMINISTRATOR));

        userRepo.save(user);

        return "redirect:/main";
    }
}
