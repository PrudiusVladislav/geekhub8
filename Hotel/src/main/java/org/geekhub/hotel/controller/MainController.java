package org.geekhub.hotel.controller;

import org.geekhub.hotel.domain.Product;
import org.geekhub.hotel.domain.User;
import org.geekhub.hotel.repository.ProductRepo;
import org.geekhub.hotel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class MainController {

    private ProductRepo productService;
    private UserService userService;


    @Autowired
    public MainController(ProductRepo productService, UserService userService) {
        this.productService = productService;
        this.userService = userService;
    }

    @GetMapping("/")
    public String home(Model model) {
        return "home";
    }


    @GetMapping("/main")
    public String main(Model model) {

        List<Product> products = (List<Product>) productService.findAll();
        List<User> users = userService.findAllUsers();

        model.addAttribute("products", products);

        return "main";
    }

//    @GetMapping("/users")
//    private String users(Model model){
//        return "redirect:/users";
//    }

    @PostMapping("/saveProduct")
    public String addProduct(Product product, Model model) {
        Product saveProduct = productService.save(product);

        List<Product> products = (List<Product>) productService.findAll();

        model.addAttribute("products", products);

        return "main";
    }
}
