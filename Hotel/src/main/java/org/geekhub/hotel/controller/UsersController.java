package org.geekhub.hotel.controller;

import org.geekhub.hotel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UsersController {

    private UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    private String getUsers(Model model){
        model.addAttribute("users",userService.findAllUsers());
        return "users";
    }
}
