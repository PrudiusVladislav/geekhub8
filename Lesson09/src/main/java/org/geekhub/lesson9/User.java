package org.geekhub.lesson9;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private final int id;
    private final String name;
    private final int accessLevel;
    private final String favoriteEncoding;
    private static final long serialVersionUID = -2018;

    public User(int id, String name, int accessLevel, String favoriteEncoding) {
        this.id = id;
        this.name = name;
        this.accessLevel = accessLevel;
        this.favoriteEncoding = favoriteEncoding;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public String getFavoriteEncoding() {
        return favoriteEncoding;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", accessLevel=" + accessLevel +
                ", favoriteEncoding='" + favoriteEncoding + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                accessLevel == user.accessLevel &&
                Objects.equals(name, user.name) &&
                Objects.equals(favoriteEncoding, user.favoriteEncoding);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, accessLevel, favoriteEncoding);
    }
}
