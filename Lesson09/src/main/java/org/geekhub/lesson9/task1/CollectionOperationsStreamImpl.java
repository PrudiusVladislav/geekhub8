package org.geekhub.lesson9.task1;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionOperationsStreamImpl implements CollectionOperations {

    @Override
    public <E> List<E> fill(Supplier<E> producer, int count) {
        return Stream
                .generate(producer)
                .limit(count)
                .collect(Collectors.toList());
    }

    @Override
    public <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        return elements.stream()
                .filter(filter)
                .collect(Collectors.toList());
    }

    @Override
    public <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        return elements.stream()
                .anyMatch(predicate);
    }

    @Override
    public <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        return elements.stream()
                .allMatch(predicate);
    }

    @Override
    public <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        return elements.stream()
                .noneMatch(predicate);
    }

    @Override
    public <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        return elements.stream()
                .map(mappingFunction)
                .collect(Collectors.toList());
    }

    @Override
    public <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        return elements.stream()
                .max(comparator);
    }

    @Override
    public <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        return elements.stream()
                .min(comparator);
    }

    @Override
    public <E> List<E> distinct(List<E> elements) {
        return elements.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public <E> void forEach(List<E> elements, Consumer<E> consumer) {
        elements
                .forEach(consumer);
    }

    @Override
    public <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {

        return elements.stream()
                .reduce(accumulator);
    }

    @Override
    public <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        return elements.stream()
                .reduce(seed, accumulator);
    }

    @Override
    public <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        return elements.stream()
                .collect(Collectors.partitioningBy(predicate));
    }

    @Override
    public <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        return elements.stream()
                .collect(Collectors.groupingBy(classifier));
    }

    @Override
    public <T, K, U> Map<K, U> toMap(List<T> elements, Function<T, K> keyFunction, Function<T, U> valueFunction,
                                     BinaryOperator<U> mergeFunction) {
        return elements.stream()
                .collect(Collectors.toMap(keyFunction, valueFunction, mergeFunction));
    }
}