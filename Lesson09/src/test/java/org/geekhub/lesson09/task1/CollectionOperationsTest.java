package org.geekhub.lesson09.task1;

import org.geekhub.lesson9.task1.CollectionOperations;
import org.geekhub.lesson9.task1.CollectionOperationsStreamImpl;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;
import java.util.function.*;

public class CollectionOperationsTest {
    private CollectionOperations collectionOperations;

    @BeforeMethod
    public void setUp() {
        collectionOperations = new CollectionOperationsStreamImpl();

    }

    @Test
    public void shouldAddElement() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(1, "User1", true),
                new User(1, "User1", true)
        );
        Supplier<User> supplier = CollectionOperationsTest::getUser;

        List<User> actual = collectionOperations.fill(supplier, 3);
        Assert.assertEquals(actual, users);
    }

    @Test
    public void shouldAddZeroElement() {
        Supplier<User> supplier = CollectionOperationsTest::getUser;

        List<User> actual = collectionOperations.fill(supplier, 0);

        Assert.assertTrue(actual.isEmpty());
    }

    @Test
    public void shouldFilterList() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(2, "User1", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        Predicate<User> predicate = User::isAdmin;

        List<User> actual = collectionOperations.filter(users, predicate);
        List<User> expected = new ArrayList<>();
        expected.add(new User(1, "User1", true));
        expected.add(new User(4, "User4", true));

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldAnyMatchList() {
        List<User> users = List.of(
                new User(2, "User2", false),
                new User(1, "User1", true),
                new User(3, "User3", true),
                new User(6, "User6", false)
        );

        Predicate<User> predicate = User::isAdmin;

        Assert.assertTrue(collectionOperations.anyMatch(users, predicate));
    }

    @Test
    public void shouldNotAnyMatchList() {
        List<User> users = List.of(
                new User(2, "User1", false),
                new User(3, "User3", false)
        );

        Predicate<User> predicate = User::isAdmin;

        Assert.assertFalse(collectionOperations.anyMatch(users, predicate));
    }

    @Test
    public void shouldAllMatchList() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(2, "User1", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        Predicate<User> predicate = User::isAdmin;

        Assert.assertFalse(collectionOperations.allMatch(users, predicate));
    }


    @Test
    public void shouldNoneMatchList() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(2, "User1", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        Predicate<User> predicate = User::isAdmin;

        Assert.assertFalse(collectionOperations.noneMatch(users, predicate));
    }

    @Test
    public void shouldMapList() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(2, "User1", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        Function<User, Integer> function = User::getId;

        List<Integer> actual = collectionOperations.map(users, function);
        List<Integer> expected = List.of(1, 2, 3, 4);

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldFoundMaxElementInList() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(2, "User1", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        Comparator<User> comparator = User::compareTo;

        Optional<User> user = collectionOperations.max(users, comparator);

        Assert.assertTrue(user.isPresent());
        Assert.assertEquals(user.get().getId(), 4);

    }

    @Test
    public void shouldNotFoundMaxElementWhenCollectionIsEmpty() {
        List<User> users = new ArrayList<>();

        Comparator<User> comparator = User::compareTo;

        Optional<User> user = collectionOperations.max(users, comparator);

        Assert.assertFalse(user.isPresent());

    }

    @Test
    public void shouldFoundMinElementInList() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(2, "User1", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        Comparator<User> comparator = User::compareTo;

        Optional<User> user = collectionOperations.min(users, comparator);

        Assert.assertTrue(user.isPresent());
        Assert.assertEquals(user.get().getId(), 1);

    }

    @Test
    public void shouldNotFoundMinElementWhenCollectionIsEmpty() {
        List<User> users = new ArrayList<>();

        Comparator<User> comparator = User::compareTo;

        Optional<User> user = collectionOperations.max(users, comparator);

        Assert.assertFalse(user.isPresent());

    }

    @Test
    public void shouldReturnListOfUniqueElements() {
        List<User> notUniqueUsers = List.of(
                new User(1, "User1", true),
                new User(1, "User1", true),
                new User(3, "User3", false),
                new User(2, "User2", true));

        List<User> uniqueUsers = List.of(
                new User(1, "User1", true),
                new User(3, "User3", false),
                new User(2, "User2", true));

        List<User> actual = collectionOperations.distinct(notUniqueUsers);

        Assert.assertEquals(actual, uniqueUsers);
    }

    @Test
    public void shouldAddUserInLocalCollection() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(2, "User1", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        List<User> actualUsers = new ArrayList<>();
        Consumer<User> consumer = actualUsers::add;

        collectionOperations.forEach(users, consumer);

        Assert.assertEquals(actualUsers, users);

    }

    @Test
    public void shouldCalculatedNumber() {
        BinaryOperator<Integer> sumOperator = (number1, number2) -> number1 + number2;

        List<Integer> list = List.of(1, 2, 3, 4, 5);

        Optional<Integer> actual = collectionOperations.reduce(list, sumOperator);
        Integer expected = 15;

        Assert.assertTrue(actual.isPresent());
        Assert.assertEquals(actual.get(), expected);
    }


    @Test
    public void shouldAccumulateNumberWhenSeed_7() {
        BinaryOperator<Integer> sumOperator = (number1, number2) -> number1 + number2;

        List<Integer> list = List.of(1, 2, 3);
        Integer seed = 7;
        Integer expected = 13;

        Integer actual = collectionOperations.reduce(seed, list, sumOperator);

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldSplitElementIntoTwoGroups() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(2, "User1", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        Predicate<User> predicate = user -> user.getId() < 2;

        List<User> expectedUsersWherePredicateTrue = List.of(
                new User(1, "User1", true)
        );

        List<User> expectedUsersWherePredicateFalse = List.of(
                new User(2, "User2", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        Map<Boolean, List<User>> expected = Map.of(
                true, expectedUsersWherePredicateTrue,
                false, expectedUsersWherePredicateFalse
        );

        Map<Boolean, List<User>> actual = collectionOperations.partitionBy(users, predicate);

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldGroupUserByAdmin() {
        List<User> users = List.of(
                new User(1, "User1", true),
                new User(2, "User1", false),
                new User(3, "User3", false),
                new User(4, "User4", true)
        );

        Function<User, Boolean> function = User::isAdmin;

        List<User> expectedAdmins = List.of(
                new User(1, "User1", true),
                new User(4, "User4", true)
        );

        List<User> expectedNotAdmin = List.of(
                new User(2, "User2", false),
                new User(3, "User3", false)
        );

        Map<Boolean, List<User>> expected = Map.of(
                true, expectedAdmins,
                false, expectedNotAdmin
        );

        Map<Boolean, List<User>> actual = collectionOperations.groupBy(users, function);

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldCreateMapUsers() {
        Function<User, Integer> keyFunction = User::getId;

        Function<User, String> valueFunction = User::getName;

        BinaryOperator<String> mergeFunction = (s, s2) -> s + s2;

        List<User> users = List.of(
                new User(2, "User2", false),
                new User(3, "User3", false),
                new User(2, "User2", false)
        );

        Map<Integer, String> expected = Map.of(
                2, "User2User2",
                3, "User3"
        );

        Map<Integer, String> actual = collectionOperations.toMap(users, keyFunction, valueFunction, mergeFunction);

        Assert.assertEquals(actual, expected);
    }


    private static User getUser() {
        return new User(1, "user1", true);
    }

}