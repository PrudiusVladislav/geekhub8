package org.geekhub.lesson09.task1;

import java.util.Objects;

public class User implements Comparable<User>{
    private int id;
    private String name;
    private boolean admin;

    public User(int id, String name, boolean admin) {
        this.id = id;
        this.name = name;
        this.admin = admin;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isAdmin() {
        return admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", admin=" + admin +
                '}';
    }

    @Override
    public int compareTo(User user) {
        return this.getId() > user.getId() ? 1 : -1;
    }
}
