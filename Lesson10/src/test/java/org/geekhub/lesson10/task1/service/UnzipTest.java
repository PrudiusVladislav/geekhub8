package org.geekhub.lesson10.task1.service;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class UnzipTest {

    @Test
    public void testUnZipАrchive() {
        Unzip unzip = new Unzip();

        Path pathIn = Paths.get("src/main/resources/download/users.zip");
        Path pathOut = Paths.get("src/main/resources/");

        unzip.unZip(pathIn, pathOut);

        Assert.assertTrue(Files.exists(Paths.get("src/main/resources/dir/")));

    }
}