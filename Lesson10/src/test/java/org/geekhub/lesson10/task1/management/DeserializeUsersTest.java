package org.geekhub.lesson10.task1.management;

import org.geekhub.lesson9.User;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class DeserializeUsersTest {

    @Test
    public void shouldDeserializeUsers() {
        DeserializeUsers deserializeUsers = new DeserializeUsers();

        Path pathToUsersFile = Paths.get("src/main/resources/dir/dir/users.dat");

        List<User> actual = deserializeUsers.deserialize(pathToUsersFile);
        List<User> expected = getUsers();

        Assert.assertEquals(actual, expected);
    }

    private static List<User> getUsers() {
       return List.of(new User(1,"Alfa Centaurus", 10, "WTF"),
                new User(2,"Combat", 50, "QTF"),
                new User(3,"Johny", 45, "UTF"),
                new User(4,"Simona", 18, "UTF"),
                new User(5,"Alex", 0, "WIN"),
                new User(6,"Omega", 45, "ISO"),
                new User(7,"Charlie", 25, "WIN"),
                new User(16,"Bravo", 45, "UTF"),
                new User(9,"Omega", 45, "UTF"),
                new User(10,"Delta", 22, "WIN"),
                new User(11,"Tip", 13, "UTF"),
                new User(12,"Zorge", 23, "ISO"),
                new User(13,"Richard", 13, "WTF"),
                new User(14,"Zulu", 45, "UTF"),
                new User(15,"Yellow", 30, "UTF"),
                new User(8,"Cassius", 80, "UTF")
                );
    }
}