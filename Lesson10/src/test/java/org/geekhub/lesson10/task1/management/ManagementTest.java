package org.geekhub.lesson10.task1.management;

import org.geekhub.lesson9.User;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.file.Paths;
import java.util.List;

public class ManagementTest {
    private Management management;

    @BeforeMethod
    public void setUp() {
        management = new Management();
    }

    @Test
    public void shouldFindFavoriteEncoding() {
        DeserializeUsers deserializeUsers = new DeserializeUsers();

        String actual = management.findFavoriteEncoding(deserializeUsers.deserialize(Paths.get("src/main/resources/dir/dir/users.dat")));
        String expected = "UTF";
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldFindAverageAccess() {
        DeserializeUsers deserializeUsers = new DeserializeUsers();

        Double actual = management.getAverageAccess(deserializeUsers.deserialize(Paths.get("src/main/resources/dir/dir/users.dat")));
        Double expected = 31.8125;

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldFindIdUserWithBiggestNameLength() {
        DeserializeUsers deserializeUsers = new DeserializeUsers();

        int actual = management.getIdUserWithBiggestNameLength(deserializeUsers.deserialize(Paths.get("src/main/resources/dir/dir/users.dat")), 31.8125);
        int expected = 8;

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldWriteUsersNameInTempFileWithEncodingUTF8() {
        DeserializeUsers deserializeUsers = new DeserializeUsers();
        List<User> users = deserializeUsers.deserialize(Paths.get("src/main/resources/dir/dir/users.dat"));
        long actual = management.writeUsersNameInTempFile(users, Paths.get("src/main/resources/"), "UTF", 8);
        long expected = 126;

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void shouldWriteUsersNameInTempFileWithEncodingUTF16() {
        DeserializeUsers deserializeUsers = new DeserializeUsers();
        List<User> users = deserializeUsers.deserialize(Paths.get("src/main/resources/dir/dir/users.dat"));
        long actual = management.writeUsersNameInTempFile(users, Paths.get("src/main/resources/"), "UTF", 16);
        long expected = 254;

        Assert.assertEquals(actual, expected);
    }
}