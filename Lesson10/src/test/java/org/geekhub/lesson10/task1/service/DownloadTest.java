package org.geekhub.lesson10.task1.service;

import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DownloadTest {

    @Test
    public void shouldDownloadFile() {
        Download download = new Download();
        long actual = 0;
        long expected = 0;
        try {
             actual = download.downloadFile(new URL("https://gitlab.com/olexandr.kucher/geekhub8/raw/prod/Lesson10/src/main/resources/users.zip"),
                    Paths.get("src/main/resources/download/users.zip"));

            expected = Files.size(Paths.get("src/test/resources/users.zip"));

        } catch (IOException e) {
           throw new RuntimeException(e);
        }

        Assert.assertEquals(actual, expected);
    }
}