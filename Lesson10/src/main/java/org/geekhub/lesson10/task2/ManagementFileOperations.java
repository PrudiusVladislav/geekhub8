package org.geekhub.lesson10.task2;

import java.nio.file.Path;

public class ManagementFileOperations {

    private static void getFileOperation(Integer number, Path path, String newName) {
        FileOperations fileOperations = new FileOperationsImpl();
        switch (number) {
            case 1:
                fileOperations.createFile(path);
                break;
            case 2:
                fileOperations.deleteFile(path);
                break;
            case 3:
                fileOperations.renameFile(path, newName);
                break;
        }
    }
}
