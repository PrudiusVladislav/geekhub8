package org.geekhub.lesson10.task2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileOperationsImpl implements FileOperations {
    @Override
    public boolean createFile(Path path) {
        if (!path.toFile().exists()) {
            try {
                Files.createFile(path);
                return true;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return false;
    }

    @Override
    public boolean deleteFile(Path path) {
        if(path.toFile().exists()) {
            try {
                Files.delete(path);
                return true;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return false;
    }

    @Override
    public boolean renameFile(Path path, String newName) {
        if(path.toFile().exists()) {
            try {
                Files.move(path, path.resolveSibling(newName));
                return true;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return false;
    }
}
