package org.geekhub.lesson10.task2;

import java.nio.file.Path;

public interface FileOperations {
    boolean createFile(Path path);

    boolean deleteFile(Path path);

    boolean renameFile(Path path, String newName);

}
