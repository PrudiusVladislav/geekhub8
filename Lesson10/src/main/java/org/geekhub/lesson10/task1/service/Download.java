package org.geekhub.lesson10.task1.service;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Download {
    public long downloadFile(URL url, Path path) {
        createDirectory();
        long copy;
        try (InputStream is = url.openStream()) {
            copy = Files.copy(is, path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return copy;
    }

    private static void createDirectory() {
        if (!Files.exists(Paths.get("src/main/resources/download"))) {
            try {
                Files.createDirectory(Paths.get("src/main/resources/download"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}