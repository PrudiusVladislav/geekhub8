package org.geekhub.lesson10.task1.service;

import java.io.File;
import java.nio.file.Path;
import java.util.Objects;

public class Finder {

    public File findFile(Path path, String name) {
        File[] files = path.toFile().listFiles();
        if (Objects.nonNull(files)) {
            for (File file : files) {
                if (file.isDirectory()) {
                    return findFile(file.toPath(), name);
                }
                if (file.toString().endsWith(name)) {
                    return file;
                }
            }
        }
        return null;
    }
}
