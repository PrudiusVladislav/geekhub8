package org.geekhub.lesson10.task1.service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Unzip {
    public void unZip(Path in, Path out) {
        byte[] buffer = new byte[2048];
        try (FileInputStream fis = new FileInputStream(in.toString());
             BufferedInputStream bis = new BufferedInputStream(fis);
             ZipInputStream stream = new ZipInputStream(bis)) {

            ZipEntry entry;

            while ((entry = stream.getNextEntry()) != null) {
                if (!entry.isDirectory()) {
                        File filePath = createFile(out, entry.getName());
                    try (FileOutputStream fos = new FileOutputStream(filePath);
                         BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length)) {

                        int len;
                        while ((len = stream.read(buffer)) > 0) {
                            bos.write(buffer, 0, len);
                        }
                    }
                } else {
                    if(!Files.exists(out.resolve(entry.getName()))) {
                        createDirectory(out, entry.getName());
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static File createFile(Path out, String name) {
        return out.resolve(name).toFile();

    }

    private static Path createDirectory(Path out, String name) throws IOException {
        return Files.createDirectory(out.resolve(name));
    }



}
