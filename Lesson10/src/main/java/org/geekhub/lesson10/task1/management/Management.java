package org.geekhub.lesson10.task1.management;

import org.geekhub.lesson9.User;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Management {

    public String findFavoriteEncoding(List<User> users) {
        Map<String, Integer> map = new HashMap<>();
        String encoding = "";

        for (User user : users) {
            if (map.containsKey(user.getFavoriteEncoding())) {
                int count = map.get(user.getFavoriteEncoding());
                map.put(user.getFavoriteEncoding(), ++count);
            } else {
                map.put(user.getFavoriteEncoding(), 1);
            }
        }

        Collection<Integer> values = map.values();

        Optional<Integer> max = values.stream().max(Integer::compareTo);

        if (max.isPresent()) {
            encoding = map.entrySet()
                    .stream()
                    .filter(entry -> max.get().equals(entry.getValue()))
                    .map(Map.Entry::getKey)
                    .findFirst().get();
        }
        return encoding;
    }

    public Double getAverageAccess(List<User> users) {
        Optional<Double> average = Optional.of(users.stream()
                .mapToInt(User::getAccessLevel).average().getAsDouble());

        return average.get();
    }

    public int getIdUserWithBiggestNameLength(List<User> users, double average) {
        Optional<User> userWith = users.stream()
                .filter(user -> user.getAccessLevel() > average)
                .max(Comparator.comparingInt(value -> value.getName().length()));

        int id = 0;

        if (userWith.isPresent()) {
            id = userWith.get().getId();
        }

        return id;
    }

    public long writeUsersNameInTempFile(List<User> users, Path path, String according, int targetUserId) {
        Charset charset = Charset.forName(according + targetUserId);
        Path file;
        try {
             file = Files.createTempFile(path, "file", ".txt");

            try (BufferedWriter writer = Files.newBufferedWriter(
                    file, charset)) {
                for (User user : users) {
                    writer.write(user.getName() + "\r\n");
                }

            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
      return file.toFile().length();
    }
}
