package org.geekhub.lesson10.task1.management;

import org.geekhub.lesson9.User;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class DeserializeUsers {
    public List<User> deserialize(Path pathToUsersFile) {
        List<User> users = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(pathToUsersFile.toFile());
             BufferedInputStream bis = new BufferedInputStream(fis);
             ObjectInputStream in = new ObjectInputStream(bis)) {

            while (true) {
                Object object = in.readObject();
                if (object instanceof User) {
                    users.add((User) object);
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            return users;
        }
    }
}
