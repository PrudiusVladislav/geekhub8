package org.geekhub.lesson10.task3;

import java.io.File;

public interface CopyFile {
    long copyWithBuffer(File sourceFile, File copyFile);

    long copyWithoutBuffer(File sourceFile, File copyFile);
}
