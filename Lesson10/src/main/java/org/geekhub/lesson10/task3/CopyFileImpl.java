package org.geekhub.lesson10.task3;

import java.io.*;
import java.nio.file.Files;

public class CopyFileImpl implements CopyFile {
    @Override
    public long copyWithBuffer(File sourceFile, File copyFile) {
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(sourceFile));
             BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(copyFile))
        ) {
            byte[] buffer = bis.readAllBytes();
            bos.write(buffer, 0, buffer.length);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return copyFile.length();
    }

    @Override
    public long copyWithoutBuffer(File sourceFile, File copyFile) {
        try {
            Files.copy(sourceFile.toPath(), copyFile.toPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return copyFile.length();
    }
}
