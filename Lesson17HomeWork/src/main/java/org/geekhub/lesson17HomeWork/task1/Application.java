package org.geekhub.lesson17HomeWork.task1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;


@SpringBootApplication
@PropertySource("classpath:application.yml")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
        final DataSourceInitializer initializer = new DataSourceInitializer();
        initializer.setDataSource(dataSource);
        initializer.setDatabasePopulator(databasePopulator());
        return initializer;
    }

    private DatabasePopulator databasePopulator() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO users(id, admin, first_name, last_name, login, password) VALUES(1, true, 'admin', 'admin'," +
                " 'admin', 'admin') ON CONFLICT (id) DO UPDATE SET admin=true, first_name='admin', last_name = 'admin'," +
                "login='admin', password='admin';");

        final ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        String sql = sb.toString();

        Resource script = new ByteArrayResource(sql.getBytes());
        populator.addScript(script);

        return populator;
    }
}
