package org.geekhub.lesson17HomeWork.task1.controllers;

import org.geekhub.lesson17HomeWork.task1.dto.User;
import org.geekhub.lesson17HomeWork.task1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HomeRestController {

    private UserRepository userRepository;

    @Autowired
    public HomeRestController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/rest/allUsers")
    public List<User> getAllUsers(){
        return (List) userRepository.findAll();
    }

    @GetMapping("/rest/user")
    public User getUser(Integer id) {
        return userRepository.findById(id);
    }
}
