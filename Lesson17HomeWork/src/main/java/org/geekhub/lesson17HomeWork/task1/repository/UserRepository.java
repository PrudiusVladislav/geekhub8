package org.geekhub.lesson17HomeWork.task1.repository;

import org.geekhub.lesson17HomeWork.task1.dto.User;
import org.geekhub.lesson17HomeWork.task1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    private UserService userService;

    @Autowired
    public UserRepository(UserService userService) {
        this.userService = userService;
    }

    public User findByLogin(String login) {
        return userService.findByLogin(login);
    }

    public User findById(Integer id) {
        return userService.findById(id);
    }

    public List<User> findAll() {
        return userService.findAll();
    }

    public void delete(User user) {
        userService.delete(user);
    }

    public int update(User user) {
        return userService.update(user);
    }

    public int save(User user) {
        return userService.save(user);
    }
}