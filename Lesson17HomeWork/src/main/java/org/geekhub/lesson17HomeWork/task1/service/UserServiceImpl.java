package org.geekhub.lesson17HomeWork.task1.service;

import org.geekhub.lesson17HomeWork.task1.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserServiceImpl implements UserService {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public UserServiceImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public User findByLogin(String login) {
        return jdbcTemplate.queryForObject(
                "SELECT * FROM users WHERE login = :userLogin",
                new MapSqlParameterSource("userLogin", login),
                new BeanPropertyRowMapper<>(User.class)
        );

    }

    @Override
    public User findById(Integer id) {
        return jdbcTemplate.queryForObject(
                "SELECT * FROM users WHERE id = :userId",
                new MapSqlParameterSource("userId", id),
                new BeanPropertyRowMapper<>(User.class)
        );

    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query("SELECT * FROM users",
                new BeanPropertyRowMapper<>(User.class)
        );
    }

    @Override
    public void delete(User user) {
        jdbcTemplate.update("DELETE FROM users WHERE id=:userId",
                new MapSqlParameterSource("userId", user.getId())
        );
    }

    @Override
    public int update(User user) {
        return jdbcTemplate.update("UPDATE users SET login=:login, password=:password, first_name=:first_name, last_name=:last_name, admin=:admin WHERE id= :userId",
                new MapSqlParameterSource("login", user.getLogin())
                        .addValue("password", user.getPassword())
                        .addValue("first_name", user.getFirstName())
                        .addValue("last_name", user.getLastName())
                        .addValue("admin", user.getAdmin())
                        .addValue("userId", user.getId())
        );
    }

    @Override
    public int save(User user) {
        return jdbcTemplate.update("INSERT INTO USERS(login, password, first_name, last_name, admin) VALUES (:login, :password, :first_name, :last_name, :admin)",
                new MapSqlParameterSource("admin", user.getAdmin())
                        .addValue("login", user.getLogin())
                        .addValue("password", user.getPassword())
                        .addValue("first_name", user.getFirstName())
                        .addValue("last_name", user.getLastName())

        );

    }
}
