package org.geekhub.lesson17HomeWork.task1.controllers;

import org.geekhub.lesson17HomeWork.task1.dto.User;
import org.geekhub.lesson17HomeWork.task1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class LoginController {
    private UserRepository userRepository;

    @Autowired
    public LoginController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

        @GetMapping("/authentication")
    public String authentication() {
        return "authentication";
    }

    @PostMapping()
    public String validationUser(User user, Model model) {

        User userByLogin = userRepository.findByLogin(user.getLogin());

        List<User> users = userRepository.findAll();

        model.addAttribute("users", users);

        if (userByLogin.getAdmin()) {
            return "main";
        }

        return "authentication";
    }
}
