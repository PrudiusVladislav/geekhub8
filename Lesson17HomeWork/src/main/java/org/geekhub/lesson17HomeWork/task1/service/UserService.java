package org.geekhub.lesson17HomeWork.task1.service;

import org.geekhub.lesson17HomeWork.task1.dto.User;

import java.util.List;

public interface UserService {

    User findByLogin(String login);

    User findById(Integer id);

    List<User> findAll();

    void delete(User user);

    int update(User user);

    int save(User user);
}
