<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Panel</title>
</head>
<body>
<form  method='get'  action='panel' >
<fieldset>
    <legend>Add personal information:</legend>

        Login:<br>
        <input type='text' name='login'><br>
        Password:<br>
        <input type='text' name='password'><br>
        First name:<br>
        <input type='text' name='firstName'><br>
        Last name:<br>
        <input type='text' name='lastName'><br>
        Is admin:
        Yes:<input type='radio' name='admin' value='on'>
        No:<input type='radio' name='admin' value='off' checked>
        <br>
        <input type='submit' value='Apply'>

</fieldset>
</form>
<br>
<form method='GET' action="/delete">
<fieldset>
    <legend>All users:</legend>
    <table border="1">
        <td>Id</td>
        <td>Login</td>
        <td>Password</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Is admin</td>
        <td>Action</td>
        <tr>
        <c:forEach var="users" items="${users}">
            <td>${users.id}</td>
            <td>${users.login}</td>
            <td>${users.password}</td>
            <td>${users.firstName}</td>
            <td>${users.lastName}</td>
            <td>${users.admin}</td>
            <td> <button type='submit' name='id' value='${users.id}'>Delete</button></td>
            <tr>
        </c:forEach>
    </table>

</fieldset>
</form>
</body>
</html>
