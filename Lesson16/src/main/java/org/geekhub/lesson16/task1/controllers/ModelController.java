package org.geekhub.lesson16.task1.controllers;

import org.geekhub.lesson15.task1.model.Source;
import org.geekhub.lesson16.task1.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ModelController {
    @Autowired
    Source source;

    @RequestMapping(value = {"/panel"})
    public String byStringViewName() {
        return "panel";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteUser(@RequestParam("id") String id ,  Model model){
        source.delete(id);
        model.addAttribute("users", source.getAllUser());
        return "panel";
    }

    @GetMapping(value = "panel")
    public String addUser(User user, Model model){
        source.add(
                user.getLogin(),
                user.getPassword(),
                user.getFirstName(),
                user.getLastName(),
                user.getAdmin()
        );

        model.addAttribute("users", source.getAllUser());
        return "panel";
    }
}
