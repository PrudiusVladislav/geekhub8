package org.geekhub.lesson16.task1.interceptors;

import org.geekhub.lesson10.logger.ConsoleLogger;
import org.geekhub.lesson10.logger.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class TimeInterceptor extends HandlerInterceptorAdapter {
    Logger logger = new ConsoleLogger(TimeInterceptor.class);
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.logInfo("Start - " + System.currentTimeMillis());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        logger.logInfo("Finish - " + System.currentTimeMillis());
    }
}
