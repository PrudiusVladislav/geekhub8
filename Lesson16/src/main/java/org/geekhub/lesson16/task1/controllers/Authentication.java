package org.geekhub.lesson16.task1.controllers;

import org.geekhub.lesson15.task1.model.Source;
import org.geekhub.lesson16.task1.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class Authentication {
    @Autowired
    Source source;

    @RequestMapping(value = {"/home"})
    public String byStringViewName() {
        return "home";
    }

    @PostMapping("/home")
    public String getParams(User user, Model model) {
        if(source.contains(user.getLogin(), user.getPassword())) {
            if(source.isAdmin(user.getLogin(), user.getPassword())) {
                model.addAttribute("users", source.getAllUser());
                return "panel";
            }
        }
        return "User not admin";
    }

}
