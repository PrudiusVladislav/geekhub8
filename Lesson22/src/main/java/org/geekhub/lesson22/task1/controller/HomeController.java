package org.geekhub.lesson22.task1.controller;

import org.geekhub.lesson10.logger.ConsoleLogger;
import org.geekhub.lesson10.logger.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

   Logger logger =  new ConsoleLogger(HomeController.class);

    @GetMapping("/")
    public String main(String url, Integer thread) {
    return "main";
    }

    @PostMapping("main")
    public String findImage(String url, Integer thread, Model model) {

        try {
            Document document = Jsoup.connect(url).get();
            Elements imgs = document.select("a");
            List<String> links = new ArrayList<>();
            for (Element element: imgs) {
                System.out.println(element.attr("href"));
                links.add(element.attr("href"));
            }
        model.addAttribute("imgs", links);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "main";
    }

    @GetMapping("img")
    public String img() {

        return "main";
    }


    @GetMapping("error")
    public String exception() {
        if (true) {
            throw new RuntimeException("Not found page");
        }
        return "error";
    }
}
