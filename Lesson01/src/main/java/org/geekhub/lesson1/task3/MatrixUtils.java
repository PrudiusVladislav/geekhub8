package org.geekhub.lesson1.task3;

import java.util.Arrays;

public class MatrixUtils {
    public static void main(String[] args) {
        int[][] matrix = createMatrix(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        fillMatrix(matrix, 1);
        printMatrix(matrix);
        int[] roots = findCellsWithIntegerSquareRootValue(matrix);
        printCellsWithIntegerSquareRootValue(matrix, roots);
        sumRootsOfCellsWithIntegerSquareRoot(matrix);
        printSumRootsOfCellsWithIntegerSquareRootInReverseOrder(roots);
    }

    static int[][] createMatrix(int rowsCount, int columnsCount) {
        return new int[rowsCount][columnsCount];
    }

    static void fillMatrix(int[][] matrix, int increment) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = increment++;
            }
        }
    }

    static int[] findCellsWithIntegerSquareRootValue(int[][] matrix) {
        int[] roots = new int[10];
        int count = 0;
        for (int i = matrix.length - 1; i >= 0; i--) {
            for (int j = matrix[i].length - 1; j >= 0; j--) {
                if (Math.sqrt(matrix[i][j]) % 1 == 0) {
                    roots[count++] = matrix[i][j];
                }
            }
        }
        return Arrays.copyOf(roots, count);
    }

    static int sumRootsOfCellsWithIntegerSquareRoot(int[][] matrix) {
        int sumRoots = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (Math.sqrt(matrix[i][j]) % 1 == 0) {
                    sumRoots += (int) Math.sqrt(matrix[i][j]);
                }
            }
        }
        return sumRoots;
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }

    private static void printCellsWithIntegerSquareRootValue(int[][] matrix, int[] roots) {
        for (int i = 0; i < roots.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                for (int k = 0; k < matrix[j].length; k++) {
                    if (matrix[j][k] == roots[i]) {
                        System.out.println("cell[" + j + "," + k + "] has value: " + roots[i] + ", It`s root: " + i);
                    }
                }
            }
        }
    }

    private static void printSumRootsOfCellsWithIntegerSquareRootInReverseOrder(int[] roots) {
        StringBuilder total = new StringBuilder();
        total.append("Total: ");
        int sumRoots = 0;
        for (int i = roots.length; i > 0; i--) {
            sumRoots += i;
            total.append(i).append("+");
        }
        total.append(" = ").append(sumRoots);
        total.deleteCharAt(total.lastIndexOf("+"));
        System.out.println(total);
    }
}