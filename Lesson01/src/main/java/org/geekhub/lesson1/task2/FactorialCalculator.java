package org.geekhub.lesson1.task2;

import java.util.Scanner;

public class FactorialCalculator {
    public static void main(String[] args) {
        if (checkInputNumber(Integer.parseInt(args[0]))) {
            System.out.println(calculateFactorialOf(Integer.parseInt(args[0])));
        } else {
            System.exit(-1);
        }
    }

    static int calculateFactorialOf(int number) {
        int result = 1;
        for (int i = 1; i <= number; i++) {
            result *= i;
        }
        return result;
    }

    private static boolean checkInputNumber(int number) {
        Scanner scan = new Scanner(System.in);
        if (number <= 0) {
            System.out.println("Error. Pleas try again");
            return false;
        } else if (number < 10) {
            return true;
        } else {
            System.out.println("This number too big. Are you assured? 1/0");
            if (scan.nextInt() == 1) {
                return true;
            } else {
                return false;
            }
        }
    }
}