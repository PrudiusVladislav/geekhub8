package org.geekhub.lesson1.task1;

import java.util.Arrays;

public class PrimeNumbersCalculator {
    public static void main(String[] args) {

        if (Integer.parseInt(args[0]) <= 0) {
            System.out.println("Error. Input non-positive number");
            System.exit(-1);
        } else {
            printPrimeNumbersInReverseOrder(findPrimeNumbersLessThan(Integer.parseInt(args[0])));
        }
    }

    static int[] findPrimeNumbersLessThan(int number) {

        if (number <= 1) {
            return new int[0];
        }

        int[] arr = new int[number];
        int countPrimeNumbers = 0;

        for (int i = 1; i < number; i++) {
            for (int j = 2; j <= i; j++) {
                if ((i == j) || (i == 2) || (j > Math.sqrt(i))) {
                    arr[countPrimeNumbers++] = i;
                    break;
                } else if (i % j == 0) {
                    break;
                }
            }
        }

        return Arrays.copyOf(arr, countPrimeNumbers);
    }

    private static void printPrimeNumbersInReverseOrder(int[] primeNumbers) {

        for (int i = primeNumbers.length - 1; i >= 0; i--) {
            System.out.println(primeNumbers[i]);
        }
    }
}