package org.trello.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.trello.model.Container;
import org.trello.service.ContainerService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/trello/")
public class ContainerRestController {

    private ContainerService containerService;

    @Autowired
    public ContainerRestController(ContainerService containerService) {
        this.containerService = containerService;
    }

    @RequestMapping
    public ResponseEntity<List<Container>> getAllContainers() {
        List<Container> containers = containerService.getAll();

        if (containers.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(containers, HttpStatus.OK);
    }

    @RequestMapping(value = "/update/{id}/{name}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Container> updateNameOfContainer(@PathVariable("id") Long id, @PathVariable("name") String name) {

        Optional<Container> byId = containerService.getById(id);

        if (byId.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Container container = byId.get();

        container.setName(name);

        containerService.update(container);

        return new ResponseEntity<>(container, HttpStatus.OK);
    }

    @RequestMapping(value = "/create/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Container> createNewContainer(@PathVariable("name") String name) {

        Optional<Container> container = containerService.getByName(name);

        if (container.isPresent()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        Container newContainer = new Container();
        newContainer.setName(name);

        containerService.save(newContainer);

        return new ResponseEntity<>(newContainer, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Container> deleteContainer(@PathVariable("id") Long id) {

        Optional<Container> container = containerService.getById(id);

        if (container.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        containerService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}