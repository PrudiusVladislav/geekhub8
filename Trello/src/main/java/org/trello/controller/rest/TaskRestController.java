package org.trello.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.trello.model.Container;
import org.trello.model.Task;
import org.trello.service.ContainerService;
import org.trello.service.TaskService;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/trello/task/")
public class TaskRestController {

    private TaskService taskService;
    private ContainerService containerService;

    @Autowired
    public TaskRestController(TaskService taskService, ContainerService containerService) {
        this.taskService = taskService;
        this.containerService = containerService;
    }

    @RequestMapping
    public ResponseEntity<List<Task>> getAllTask() {
        List<Task> tasks = taskService.getAll();

        if (tasks.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Task> getTask(@PathVariable("id") Long taskId) {
        if (taskId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<Task> task = taskService.getById(taskId);

        if (task.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(task.get(), HttpStatus.OK);

    }

    @RequestMapping(value = "/create/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Task> saveTask(@PathVariable("id") Long id, Task task) {
        Optional<Container> byId = containerService.getById(id);

        if (byId.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        task.setContainer_id(id);

        taskService.save(task);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Task> update(@PathVariable("id") Long id, Task task) {
        Optional<Task> byId = taskService.getById(id);

        if (byId.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        taskService.update(task);

        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Task> delete(@PathVariable("id") Long id) {
        Optional<Task> byId = taskService.getById(id);

        if (byId.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        taskService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}