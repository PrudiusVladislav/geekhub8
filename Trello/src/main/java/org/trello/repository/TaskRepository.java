package org.trello.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.trello.model.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {
}