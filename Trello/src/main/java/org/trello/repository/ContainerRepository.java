package org.trello.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.trello.model.Container;

import java.util.Optional;

public interface ContainerRepository extends JpaRepository<Container, Long> {
    Optional<Container> findByName(String name);
}