package org.trello.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trello.model.Container;
import org.trello.repository.ContainerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ContainerServiceImpl implements ContainerService {

    private ContainerRepository containerRepository;

    @Autowired
    public ContainerServiceImpl(ContainerRepository containerRepository) {
        this.containerRepository = containerRepository;
    }

    @Override
    public void save(Container container) {
        containerRepository.save(container);
    }

    @Override
    public void delete(Long id) {
        containerRepository.deleteById(id);
    }

    @Override
    public List<Container> getAll() {
        return containerRepository.findAll();
    }

    @Override
    public Optional<Container> getById(Long id) {
        return containerRepository.findById(id);
    }

    @Override
    public void update(Container container) {
        containerRepository.save(container);
    }

    @Override
    public Optional<Container> getByName(String name) {
        return containerRepository.findByName(name);
    }
}