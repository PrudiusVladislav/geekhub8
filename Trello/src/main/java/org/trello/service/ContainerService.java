package org.trello.service;

import org.trello.model.Container;

import java.util.List;
import java.util.Optional;

public interface ContainerService {

    Optional<Container> getById(Long id);

    Optional<Container> getByName(String name);

    void save(Container container);

    void delete(Long id);

    List<Container> getAll();

    void update(Container container);
}