
create sequence hibernate_sequence start with 1 increment by 1;

create table Container (
                         container_id bigint not null,
                         name varchar(255),
                         primary key (container_id)
);



create table Task (
                    id bigint not null,
                    container_id bigint,
                    datetime varchar(255),
                    description varchar(255),
                    name varchar(255),
                    primary key (id)
);

alter table Task
  add constraint FKiytyrobf117w7hccax0xlwt3b
    foreign key (container_id)
      references Container