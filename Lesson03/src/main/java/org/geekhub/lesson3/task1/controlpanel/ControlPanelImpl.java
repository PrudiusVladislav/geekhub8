package org.geekhub.lesson3.task1.controlpanel;

import org.geekhub.lesson3.task1.drivable.Drivable;

public class ControlPanelImpl implements ControlPanel {

    @Override
    public void turnLeft(Drivable car) {
        car.turnLeft();
    }

    @Override
    public void turnRight(Drivable car) {
        car.turnRight();
    }

    @Override
    public int accelerate(Drivable car) {
        car.startTheEngine();
        return car.accelerate();
    }

    @Override
    public int brake(Drivable car) {
        return car.brake();
    }

    @Override
    public void fillTank(Drivable car, int i) {
        car.fillTank(i);
    }
}
