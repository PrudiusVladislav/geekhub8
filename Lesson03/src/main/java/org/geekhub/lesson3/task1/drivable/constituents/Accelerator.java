package org.geekhub.lesson3.task1.drivable.constituents;

public class Accelerator {
    private double accelerateStrength;
    private int accelerate;

    public Accelerator(int accelerateStrength) {
        this.accelerateStrength = accelerateStrength;
    }

    public double getAccelerateStrength() {
        if (accelerateStrength >= 25) {
            return 25;
        } else if (accelerateStrength <= 5) {
            return 5;
        }
        return accelerateStrength;
    }

    public double accelerate(Engine engine, GasTank gasTank) {
        int currentFuel = 0;
        if (gasTank.isEmpty()) {
            return 0;
        }
        if (!engine.isStarted()) {
            return 0;
        }

        for (int i = 0; i < 5; i++) {
            currentFuel += engine.getCapacity();
            if (currentFuel <= gasTank.getCurrentVolume()) {
                accelerate += accelerateStrength;
            }
        }
        return accelerate;
    }

}
