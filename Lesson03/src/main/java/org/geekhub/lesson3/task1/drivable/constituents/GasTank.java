package org.geekhub.lesson3.task1.drivable.constituents;

public class GasTank implements StatusAware {
    private double maxVolume;
    private double currentVolume;

    public GasTank(int maxVolume) {
        this.maxVolume = maxVolume;
    }

    public double getMaxVolume() {
        if (maxVolume < 5) {
            return 5;
        } else if (maxVolume > 100) {
            return 100;
        }
        return maxVolume;
    }

    public double getCurrentVolume() {
        return currentVolume;
    }

    public void use(double useFuel) {
        if (useFuel > 0) {
            if (useFuel < currentVolume) {
                currentVolume -= useFuel;
            } else {
                currentVolume = 0;
            }
        }
    }

    public void fill(double addFuel) {
        if (addFuel > 0) {
            if (addFuel > maxVolume) {
                currentVolume += maxVolume;
            } else {
                currentVolume += addFuel;
            }
        }
    }

    public String status() {

        if (currentVolume == 0) {
            return "0.0% of fuel exist";
        }
        double perCent = ((currentVolume / maxVolume) * 100);
        return perCent + "% of fuel exist";
    }

    public boolean isEmpty() {
        double limit = 0.001;
        return limit > currentVolume;
    }
}
