package org.geekhub.lesson3.task1.drivable.constituents;

public class Engine implements StatusAware {
    private double capacity;
    private int maxSpeed;
    private boolean isStarted = false;

    public Engine(double capacity, int maxSpeed) {
        this.capacity = capacity;
        this.maxSpeed = maxSpeed;
    }

    public double getCapacity() {
        if (capacity >= 6) {
            return 6;
        } else if (capacity <= 0.5) {
            return 0.5;
        }
        return capacity;
    }


    public int getMaxSpeed() {
        if (maxSpeed >= 400) {
            return 400;
        } else if (maxSpeed <= 60) {
            return 60;
        }
        return maxSpeed;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public void start() {
        isStarted = true;
    }

    public String status() {

        return isStarted ? "started" : "stopped";
    }

    public void stop() {
        isStarted = false;
    }

    public void work(GasTank tank) {
        if (isStarted) {
            tank.use(capacity);
        }
    }
}
