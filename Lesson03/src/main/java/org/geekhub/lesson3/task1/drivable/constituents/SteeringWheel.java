package org.geekhub.lesson3.task1.drivable.constituents;

public class SteeringWheel implements StatusAware {
    private int maxTurnAngle;
    private int turnAngleStep;
    private int turnAngle;
    private int maxTurnAngleLeft;

    public SteeringWheel(int maxTurnAngle, int turnAngleStep) {
        this.maxTurnAngle = maxTurnAngle;
        this.turnAngleStep = turnAngleStep;
        maxTurnAngleLeft -= maxTurnAngle;
    }

    public int getMaxTurnAngle() {
        if (maxTurnAngle > 78) {
            return 78;
        } else if (maxTurnAngle < 40) {
            return 40;
        }
        return maxTurnAngle;
    }

    public int getTurnAngleStep() {
        if (turnAngleStep > 30) {
            return 30;
        } else if (turnAngleStep < 10)
            return 10;

        return turnAngleStep;
    }

    public int getTurnAngle() {
        return turnAngle;
    }

    public void turnLeft() {
        int max = turnAngle - turnAngleStep;
        if (max < maxTurnAngleLeft) {
            turnAngle = maxTurnAngleLeft;
        } else {
            turnAngle -= turnAngleStep;
        }
    }

    public void turnRight() {
        int max = turnAngle + turnAngleStep;
        if (max > maxTurnAngle) {
            turnAngle = maxTurnAngle;
        } else {
            turnAngle += turnAngleStep;
        }
    }

    public String status() {
        return "turn angle = " + turnAngle;
    }
}
