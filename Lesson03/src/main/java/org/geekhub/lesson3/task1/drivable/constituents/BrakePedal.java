package org.geekhub.lesson3.task1.drivable.constituents;

public class BrakePedal {
    private int brakingStrength;

    public BrakePedal(int brakingStrength) {
        this.brakingStrength = brakingStrength;
    }

    public int getBrakingStrength() {
        if (brakingStrength > 25) {
            return 25;
        } else if (brakingStrength < 5) {
            return 5;
        }
        return brakingStrength;
    }
}
