package org.geekhub.lesson3.task1.drivable.cars;

import org.geekhub.lesson3.task1.drivable.Vehicle;
import org.geekhub.lesson3.task1.drivable.constituents.*;

public class Sedan extends Vehicle {
    private int numberOfDoors = 4;
    private int maxNumberOfPassengers = 5;
    private String name;

    public Sedan(String name, Accelerator accelerator, BrakePedal brakePedal, Engine engine, GasTank gasTank, SteeringWheel steeringWheel) {
        super(name, 4, 5, accelerator, brakePedal, engine, gasTank, steeringWheel);
        this.name = name;
    }


    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public int getMaxNumberOfPassengers() {
        return maxNumberOfPassengers;
    }

    public String getName() {
        return name;
    }

}
