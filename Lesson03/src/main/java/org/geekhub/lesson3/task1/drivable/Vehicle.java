package org.geekhub.lesson3.task1.drivable;

import org.geekhub.lesson3.task1.drivable.constituents.*;


public abstract class Vehicle implements Drivable {

    Accelerator accelerator;
    BrakePedal brakePedal;
    Engine engine;
    GasTank gasTank;
    SteeringWheel steeringWheel;
    String name;
    int numberOfDoors;
    int maxNumberOfPassengers;
    private int speed = 0;
    private int fuel = 0;

    public Vehicle(String name, int numberOfDoors, int maxNumberOfPassengers, Accelerator accelerator,
                   BrakePedal brakePedal, Engine engine, GasTank gasTank, SteeringWheel steeringWheel
    ) {
        this.name = name;
        this.numberOfDoors = numberOfDoors;
        this.maxNumberOfPassengers = maxNumberOfPassengers;
        this.accelerator = accelerator;
        this.brakePedal = brakePedal;
        this.engine = engine;
        this.gasTank = gasTank;
        this.steeringWheel = steeringWheel;
    }


    public void turnLeft() {
        steeringWheel.turnLeft();
    }

    public void turnRight() {
        steeringWheel.turnRight();
    }

    public void stopTheEngine() {
        engine.stop();
    }

    public void fillTank(int i) {
        gasTank.fill(i);
    }

    public int brake() {
        if (speed != 0) {
            speed -= brakePedal.getBrakingStrength();
        }
        if (speed <= 0) {
            engine.stop();
            return 0;
        }
        return speed;
    }

    @Override
    public void startTheEngine() {
        engine.start();
    }

    @Override
    public int speed() {
        return speed;
    }

    @Override
    public int accelerate() {
        if (gasTank.isEmpty()) {
            return speed;
        }
        if (engine.isStarted()) {
            for (int i = 0; i < 5; i++) {
                if (!gasTank.isEmpty()) {
                    if (speed < engine.getMaxSpeed()) {
                        speed += accelerator.getAccelerateStrength();
                        gasTank.use(engine.getCapacity());
                    }
                }
            }
        }
        return speed;
    }
}