package org.geekhub.lesson3.task1.controlpanel;

import org.geekhub.lesson3.task1.drivable.Drivable;

public interface ControlPanel {
    void turnLeft(Drivable car);

    void turnRight(Drivable car);

    int accelerate(Drivable car);

    int brake(Drivable car);

    void fillTank(Drivable car, int i);
}
