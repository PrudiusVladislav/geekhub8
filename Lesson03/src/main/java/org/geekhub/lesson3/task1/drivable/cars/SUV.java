package org.geekhub.lesson3.task1.drivable.cars;

import org.geekhub.lesson3.task1.drivable.Vehicle;
import org.geekhub.lesson3.task1.drivable.constituents.*;

public class SUV extends Vehicle {
    private int numberOfDoors = 5;
    private int maxNumberOfPassengers = 7;
    private String name;

    public SUV(String name, Accelerator accelerator, BrakePedal brakePedal, Engine engine, GasTank gasTank, SteeringWheel steeringWheel) {
        super(name, 5, 7, accelerator, brakePedal, engine, gasTank, steeringWheel);
        this.name = name;
    }


    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public int getMaxNumberOfPassengers() {
        return maxNumberOfPassengers;
    }

    public String getName() {
        return name;
    }

}
