package org.geekhub.lesson3.task1.drivable;

public interface Drivable {
    void startTheEngine();

    int speed();

    int accelerate();

    void turnLeft();

    void turnRight();

    int brake();

    void fillTank(int i);

}
