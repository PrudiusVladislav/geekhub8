package org.geekhub.lesson3.task1.drivable.constituents;

public interface StatusAware {
    public String status();
}
